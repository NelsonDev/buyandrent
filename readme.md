# BuyAndRent

BuyAndRent est une agence de ventes et de locations de biens immobiliers.

## environment de développement

### Pré-requis

* PHP 7.4
* Composer
* Symfony CLI
* Docker
* Docer-compose

Avant de commencer, vous pouvez vérifier les pré-requis (sauf Docker et Docker-compose) avec la commande suivante (de la CLI Symfony):


```bash
symfony check:requirements
```
## Lancer l'environnement de développement.

```bash
# Installer les dépendances
composer install

npm install / Yarn install

# Lancer les images docker
docker-compose up -d

# Lancer le serveur interne de Symfony
symfony serve -d

```
### Ajouter les données de tests

```bash
#Création et installation de la base de données
symfony console doctrine:database:create

#Lancer cette commande pour mettre à jour la base de données
symfony console d:s:u -f

#Plus besoin de lancer cette commande si elle a déjà été faite
symfony console doctrine:fixtures:load 
```

#### Lancer l'environnement avec Webpack

```bash
# Compiler automatiquement webpack à chaque modification
yarn dev-server

npm run  dev-server
```

