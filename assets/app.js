/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */


//Librairies permettant d'ajouter la géolocation de nos biens
import Places from 'places.js'

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';

//Importer select2
require('select2');

//Importer jQuery
const $ = require('jquery');
global.$ = global.jQuery = $;



//Import sweetalert2 pour l'affichage modal sweetalert2/dist/sweetalert2.js
import Swal from '../node_modules/sweetalert2/dist/sweetalert2.js';

//gérer les images avec la librairies
$('[data-slider]').slick({
    dots: true,
    arrow: true
})


//code pour masquer et afficher le boutton de contact
let $contactButton = $('#contactButton')
$contactButton.Onclick(event => {
    event.preventDefault();
    $('#contactForm').slideDown();
    $contactButton.slideUp();
})

// pour le chargement des cartes
import MapBien from './modules/map.js';
MapBien.getMap();


//Importer slickjs pour créer des slider pour nos images de biens
import 'slick-carousel';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

//Styliser les champs de type select avec select2
$('select').select2();

// start the Stimulus application
import './bootstrap';

//Importer les images de fontawesome 
import "../node_modules/@fortawesome/fontawesome-free/js/all.js";
import "../node_modules/@fortawesome/fontawesome-free/css/all.css";

//Importer les images de boostrap Icons
import "../node_modules/bootstrap-icons/font/bootstrap-icons.scss";


//-------------------------- Cacher la liste des produits  --------------------------
jQuery(document).ready(function() {
    // Afficher/Cacher liste des produits ( grille / Liste)
    $("#table_liste_produits").hide();
});


//------------------------------LOGITQUE POUR L'AUTOCOMPLETION DE L'ADRESSE DES BIENS--------------//

let inputAdresseBien = document.querySelector('#biens_adresse')
if (inputAdresseBien !== null) { //On vérifie si l'élement existe bien ou pas
    let place = Places({
            container: inputAdresseBien
        })
        //On gère une autocomplétion de nos champs (adresse, codePostal, long et lat) avec la librairie
    place.on('change', e => {
        document.querySelector('#biens_ville').value = e.suggestion.city
        document.querySelector('#biens_codePostal').value = e.suggestion.postcode
        document.querySelector('#biens_latitude').value = e.suggestion.latlng.lat
        document.querySelector('#biens_longitude').value = e.suggestion.latlng.lng
    })
}



//------------------------------LOGITQUE POUR L'AUTOCOMPLETION DE L'ADRESSE DES BIENS LORS DE LA RECHERCHE--------------//

let rechercheBien = document.querySelector('#recherche_bien')
if (rechercheBien !== null) {
    let place = Places({
        container: rechercheBien
    })
    place.on('change', e => {
        document.querySelector('#latitude').value = e.suggestion.latlng.lat
        document.querySelector('#longitude').value = e.suggestion.latlng.lng
    })
}




//------------------------  Side bar --------------------------------
const $button = document.querySelector('#sidebar-toggle');
const $wrapper = document.querySelector('#wrapper');

var compteur_navbar = 0;

$button.addEventListener('click', (e) => {
    e.preventDefault();
    $wrapper.classList.toggle('toggled');

    // fermé -> ouvert
    if (compteur_navbar % 2 == 0) {
        $("#logo1").removeClass('d-block');
        $("#logo2").removeClass('d-none');

        $("#logo1").addClass('d-none');
        $("#logo2").addClass('d-block');

        //infouser 
        $("#infoUser").removeClass('d-block');
        $("#infoUser").addClass('d-none');

        //linkMenu
        $("#linkMenu").addClass('linkMenu');
        $("#aMenu").addClass('d-none');
        $("#linkMenu2").addClass('linkMenu');
        $("#aMenu2").addClass('d-none');
        $("#linkMenu3").addClass('linkMenu');
        $("#aMenu3").addClass('d-none');
        $("#linkMenu4").addClass('linkMenu');
        $("#aMenu4").addClass('d-none');
        $("#linkMenu5").addClass('linkMenu');
        $("#aMenu5").addClass('d-none');


        //sidebar_nav
        $("#sidebar_nav").addClass('top120');
        compteur_navbar++;

        // ouvert->fermé
    } else {
        $("#logo1").removeClass('d-none');
        $("#logo2").removeClass('d-block');

        $("#logo1").addClass('d-block');
        $("#logo2").addClass('d-none');

        //infouser 
        $("#infoUser").removeClass('d-none');
        $("#infoUser").addClass('d-block');

        //linkMenu
        $("#linkMenu").removeClass('linkMenu');
        $("#aMenu").removeClass('d-none');
        $("#linkMenu2").removeClass('linkMenu');
        $("#aMenu2").removeClass('d-none');
        $("#linkMenu3").removeClass('linkMenu');
        $("#aMenu3").removeClass('d-none');
        $("#linkMenu4").removeClass('linkMenu');
        $("#aMenu4").removeClass('d-none');
        $("#linkMenu5").removeClass('linkMenu');
        $("#aMenu5").removeClass('d-none');

        //sidebar_nav
        $("#sidebar_nav").removeClass('top120');

        compteur_navbar++;
    }
});


//-------------------------Fermer le toaster  --------------------------
setTimeout(function() {
    $('#success-alert').fadeOut('slow');
}, 5000); // <-- time in milliseconds
document.getElementById('btn_close_toast').addEventListener('click', notifDossierClient);

function notifDossierClient() {
    $("#blockNotifDossierClient").hide();
}

//------------------------------------------- BTN LEFT RIGHT NAVBAR-----------------------
var marque_affiche = document.getElementById("sidebar-toggle");
var icone_fleche = $("#id_ico_left");
var compteur_rot_icone = 0;
marque_affiche.addEventListener("click", rotation_icone);

function rotation_icone() {

    if (compteur_rot_icone % 2 == 0) {
        icone_fleche.removeClass("bi-arrow-left-circle-fill");
        icone_fleche.addClass("bi-arrow-right-circle-fill");
        compteur_rot_icone++;
    } else {
        icone_fleche.removeClass("bi-arrow-right-circle-fill");
        icone_fleche.addClass("bi-arrow-left-circle-fill");
        compteur_rot_icone++;
    }
}

// ---------------Collapse

$(function() {
    $("#collapse").accordion({
        collapsible: true,
        active: false
    });
});
// ---------------datePicker

$(document).ready(function() {
    // you may need to change this code if you are not using Bootstrap Datepicker
    $('.js-datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });
});



//Ajouter la logique de la map qui se trouve dans le dossier modules


// function getMap() {
//     //Vérifier si la map existe o
//     let mymapBien = document.querySelector('#mymapBien')
//     if (mymapBien === null) {
//         return
//     }

//     // IMPORTANT :
//     // Pour utiliser les cartes MapBox, il faut vous inscrire sur
//     // le site web https://www.mapbox.com afin d'obtenir un token.
//     let mapboxToken = "pk.eyJ1Ijoid2ViZGV2ZWxvcCIsImEiOiJja3ppYWcyOHIyeTJpMm9vMXd2bTdqdDFpIn0.kseOK3XiUVBHLApHWEHwZg";

//     let center = [mymapBien.dataset.latBien, mymapBien.dataset.lngBien]
//     mymap = L.map("mymapBien").setView(center, 13);

//     L.tileLayer(
//         "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}", {
//             attribution: '© <a href="https://www.mapbox.com/about/maps/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> <strong><a href="https://www.mapbox.com/map-feedback/" target="_blank">Improve this map</a></strong>',
//             maxZoom: 18,
//             id: "mapbox/streets-v11",
//             tileSize: 512,
//             zoomOffset: -1,
//             accessToken: mapboxToken,
//         }
//     ).addTo(mymap);
// }
// //Appeler la fonction permettant d'afficher la map
// getMap();



//-------------------------FIN --------------------------
// document.ready(function(){

//   let current_fs, next_fs, previous_fs; 
//   let opacity;
//   let current = 1;
//   let steps = $("fieldset").length;

//   setProgressBar(current);

//   document.getElementByClassName("next").click(function(){

//   current_fs = this.parent();
//   next_fs = this.parent().nextElementSibling;


//   document.querySelectorAll("#progressbar li").eq($("fieldset").index(next_fs)).classList.add("active");


//   next_fs.style.display = "";

//   current_fs.animate({opacity: 0}, {
//   step: function(now) {

//   opacity = 1 - now;

//   current_fs.css({
//   'display': 'none',
//   'position': 'relative'
//   });
//   next_fs.css({'opacity': opacity});
//   },
//   duration: 500
//   });
//   setProgressBar(++current);
//   });

//   document.getElementByClassName("previous").click(function(){

//   current_fs = this.parent();
//   previous_fs = this.parent().previousElementSibling;


//   document.querySelectorAll("#progressbar li").eq($("fieldset").index(current_fs)).classList.remove("active");


//   previous_fs.style.display = "";


//   current_fs.animate({opacity: 0}, {
//   step: function(now) {

//   opacity = 1 - now;

//   current_fs.css({
//   'display': 'none',
//   'position': 'relative'
//   });
//   previous_fs.css({'opacity': opacity});
//   },
//   duration: 500
//   });
//   setProgressBar(--current);
//   });

//   function setProgressBar(curStep){
//   let percent = parseFloat(100 / steps) * curStep;
//   percent = percent.toFixed();
//   document.getElementByClassName("progress-bar")
//   .css("width",percent+"%")
//   }

//   document.getElementByClassName("submit").click(function(){
//   return false;
//   })

//   });