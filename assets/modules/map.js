// pour le chargement des cartes
import * as L from 'leaflet';
import 'leaflet/dist/leaflet.css';

export default class MapBien {


    static getMap() {

        // IMPORTANT :
        // Pour utiliser les cartes MapBox, il faut vous inscrire sur
        // le site web https://www.mapbox.com afin d'obtenir un token.
        let mapboxToken = 'pk.eyJ1Ijoid2ViZGV2ZWxvcCIsImEiOiJja3ppYWcyOHIyeTJpMm9vMXd2bTdqdDFpIn0.kseOK3XiUVBHLApHWEHwZg';

        //Vérifier si l'on recupère bien l'identifiant  de la map
        let mymapBien = document.querySelector('#mymapBien')
        let infoBien = mymapBien.dataset;
        if (mymapBien === null || infoBien === null) {
            return
        }

        //L'URL du marqueur de la mape
        let icon = L.icon({
            iconUrl: '/images/marker-icon.png',
        })

        //Récupérer la latitude et la longitude du bien à partir de l'attribut data-latBien et data-lngBien
        let center = [mymapBien.dataset.lat, mymapBien.dataset.lng]
        mymapBien = L.map("mymapBien").setView(center, 13);

        //Information à afficher dans la popup de la mappe


        L.tileLayer(
            "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}", {
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                maxZoom: 18,
                id: "mapbox/streets-v11",
                tileSize: 512,
                zoomOffset: -1,
                accessToken: mapboxToken,
            }
        ).addTo(mymapBien)

        //Ajouter un marqueur à notre mapboxToken
        L.marker(center, { icon: icon }).addTo(mymapBien)

        //Donner une couleur à notre cercle.
        const zones = [
            { distance: 10, color: "#00b798" },
            { distance: 30, color: "#ffb846" },
        ];

        //Ajouter un rayon à notre bien
        let zoneIndex = 0;
        L.circle(center, {
            color: zones[zoneIndex].color,
            fillColor: zones[zoneIndex].color,
            fillOpacity: 0.15,
            radius: zones[zoneIndex].distance * 100,
        }).addTo(mymapBien)


        //AJouter un pop à notre bien  ` + infoBien.titre + `   La propriété se trouve ici 
        L.popup().setLatLng(center).setContent(`
        <p style="font-weight: bold; color:blue;">TItre : <span style=" color:black">` + infoBien.titre + ` 🏠</span></p>
        <p style="font-weight: bold; color:blue;">localisation : <span style=" color:black">` + infoBien.adresse + `</p>
        <p style="font-weight: bold; color:blue;">Surface : <span style=" color:black">` + infoBien.surface + ` m²</p>
        <p style="font-weight: bold; color:blue;">Prix : <span style=" color:black">` + infoBien.prix + ` € </p>`).openOn(mymapBien);

    }

}