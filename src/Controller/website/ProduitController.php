<?php

declare(strict_types=1);

namespace App\Controller\website;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProduitController extends AbstractController
{


    /**
     * @Route("/tous-nos-produits", name="tous-nos-produits")
     */
    public function tousNosProduits(): Response
    {
        return $this->render('website/produit/tous-nos-produits.html.twig', [
            'slug' => 'tous-nos-produits'

        ]);
    }
    /**
     * @Route("/acheter", name="acheter")
     */
    public function acheter(): Response
    {
        return $this->render('website/produit/acheter.html.twig', [
            'slug' => 'acheter'
        ]);
    }

    /**
     * @Route("/louer", name="louer")
     */
    public function louer(): Response
    {
        return $this->render('website/produit/louer.html.twig', [
            'slug' => 'louer'
        ]);
    }

    /**
     * @Route("/Produit", name="produit")
     */
    public function produit(): Response
    {
        return $this->render('website/produit/produit.html.twig', [
            'slug' => 'produit'
        ]);
    }
}
