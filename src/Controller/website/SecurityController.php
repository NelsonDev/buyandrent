<?php

namespace App\Controller\website;

use App\Entity\Users;
use App\Form\InscriptionType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class SecurityController extends AbstractController
{


    /**
     * @Route("/inscription", name="inscription", methods={"GET|POST"})
     * @param Request $request
     * @return Response
     */
    public function inscription(
        Request $resquest,
        UserPasswordHasherInterface  $passwordHasher,
        EntityManagerInterface $entityManager
    ): Response {
        //Ajouter un nouveau client dans l'application
        $user = new Users();

        $form = $this->createForm(InscriptionType::class, $user);

        $form->handleRequest($resquest);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $password = $passwordHasher->hashPassword($user, $user->getpassword());
            $user->setPassword($password);
            $user->setRoles(['ROLE_USER']);
            $entityManager->persist($user);
            $entityManager->flush($user);
            $this->addFlash('success', "Votre compte a été crée 👍");


            return $this->redirectToRoute('connexion');
        }

        return $this->render('website/pages/inscription.html.twig', [
            'form' => $form->createView(),
            'slug' => 'inscription'
        ]);
    }

    /**
     * @Route("/connexion", name="connexion", methods={"GET", "POST"})
     */
    public function connexion(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('website/pages/connexion.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
            'slug' => 'connexion'
        ]);
    }

    /**
     * @Route("/deconnexion", name="deconnexion", methods={"GET"})
     */
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be 
        intercepted by the logout key on your firewall.');
    }
}
