<?php

declare(strict_types=1);

namespace App\Controller\website;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Repository\BiensRepository;
use App\Repository\AgenceRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Notification\ContactNotification;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PageController extends AbstractController
{
    private AgenceRepository $agenceRepository;

    /**
     *
     * @param AgenceRepository $agenceRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        AgenceRepository $agenceRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->agenceRepository = $agenceRepository;
        $this->entityManager = $entityManager;
    }


    /**
     *
     *@Route("/", name="home",  methods={"GET"})
     *
     * @param AgenceRepository $agenceRepository
     * @return Response
     */
    public function index(BiensRepository $biensRepository): Response
    {
        //Afficher la description de l'agence et récupérer les 8 derniers biens ajoutés
        $agence = $this->agenceRepository->findAll();

        $biens = $biensRepository->findLastproperties();

        return $this->render('website/index.html.twig', [
            'agence' => $agence[0],
            'biens' => $biens,
            'slug' => 'home' // permet d'ajouter une spécification (condition) dans la navbar

        ]);
    }


    /**
     * @Route("/contact", name="contact")
     */
    public function contact(Request $request, ContactNotification $notification): Response
    {

        //création du formulaire de contact pour l'agence
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        //traitement de la requête du formulaire
        if ($form->isSubmitted() && $form->isValid()) {
            //traiment de l'envoie dans le classe ContactNotification
            $notification->notifyAgence($contact);
            $this->addFlash('success', "Message envoyé à l'agence");
            return $this->redirectToRoute('contact');
        }


        return $this->render('website/pages/contact.html.twig', [
            'slug' => 'contact',
            'form' => $form->createView()

        ]);
    }


    /**
     * @Route("/notre-agence", name="notre-agence",methods={"GET"})
     *
     * @return Response
     */
    public function notreAgence(): Response
    {
        $agenceInfo = $this->agenceRepository->findAll();
        return $this->render('website/pages/notre-agence.html.twig', [
            'agenceInfo' => $agenceInfo[0],
            'slug' => 'notre-agence'
        ]);
    }

    /**
     * @Route("/inscription", name="inscription")
     */
    // public function inscription(): Response
    // {
    //     return $this->render('website/pages/inscription.html.twig', [
    //         'slug' => 'inscription'
    //     ]);
    // }

    /**
     * @Route("/connexion", name="connexion")
     */
    // public function connexion(): Response
    // {
    //     return $this->render('website/pages/connexion.html.twig', [
    //         'slug' => 'connexion'
    //     ]);
    // }
}
