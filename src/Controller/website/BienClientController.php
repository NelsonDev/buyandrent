<?php

declare(strict_types=1);

namespace App\Controller\website;

use App\Entity\Biens;
use App\Entity\Contact;
use App\Form\ContactType;
use App\Entity\AdminSearchBien;
use App\Form\AdminSearchBienType;
use App\Repository\BiensRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use App\Notification\ContactNotification;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BienClientController extends AbstractController
{
    private $biensRepository;
    private $entityManager;

    public function __construct(
        BiensRepository $biensRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->biensRepository = $biensRepository;
        $this->entityManager = $entityManager;
    }


    /**
     * @Route("/bien_a_vendre", name="bien_a_vendre")
     * liste function qui affiche l'ensemble des biens à vendre de l'application
     * et gérer la pagination
     *
     * @return Response
     */
    public function listeVendre(PaginatorInterface $paginator, Request $request): Response
    {
        //Ajouter un formulaire de recherche à la liste de nos biens à vendre
        $rechercheBien = new AdminSearchBien();
        $form = $this->createForm(AdminSearchBienType::class, $rechercheBien);
        $form->handleRequest($request);

        $lengthBiens = $this->biensRepository->findAllBienaVendre();

        $data = $this->biensRepository->findAllBienaVendreFormWebSite($rechercheBien);
        $biens = $paginator->paginate(
            $data, //On passe les données
            $request->query->getInt('page', 1), //Numéro de la pasge en cours, 1 par défaut
            12 //Nombre d'élement par page
        );

        return $this->render('website/produit/acheter.html.twig', [
            'slug' => 'list_bien_a_vendre',
            'biens' => $biens,
            'lengthBiens' => $lengthBiens,
            'form' => $form->createView()

        ]);
    }



    /**
     * @Route("/bien_a_louer", name="bien_a_louer")
     * liste function qui affiche l'ensemble des biens à vendre dans l'application
     * et gérer la pagination
     *
     * @return Response
     */
    public function listeLouer(PaginatorInterface $paginator, Request $request): Response
    {
        //Ajouter un formulaire de recherche à la liste de nos biens à vendre
        $rechercheBien = new AdminSearchBien();
        $form = $this->createForm(AdminSearchBienType::class, $rechercheBien);
        $form->handleRequest($request);

        $lengthBiens = $this->biensRepository->findAllBienaLouer();

        $data = $this->biensRepository->findAllBienaLouerFormWebSite($rechercheBien);
        $biens = $paginator->paginate(
            $data, //On passe les données
            $request->query->getInt('page', 1), //Numéro de la pasge en cours, 1 par défaut
            12 //Nombre d'élement par page
        );

        return $this->render('website/produit/louer.html.twig', [
            'slug' => 'list_bien_a_louer',
            'biens' => $biens,
            'lengthBiens' => $lengthBiens,
            'form' => $form->createView()

        ]);
    }

    /**
     * @Route("/detail_bien/{slugTitle}-{id}", name="detail_bien", requirements={"slugTitle": "[a-z0-9\-]*"})
     * @return Response
     */
    public function detailBien(
        Biens $bien,
        int $id,
        Request $request,
        BiensRepository $bienRepository,
        ContactNotification $notification
    ): Response {
        $similar = $bienRepository->findBySimilar(
            [
                'typeTransaction' => $bien->getTypeTransaction(),
            ],
            $id,
            4
        );

        //création du formulaire de contact pour un bien en particulier
        $contact = new Contact();
        $contact->setBien($bien);
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);


        //traitement de la requête du formulaire
        if ($form->isSubmitted() && $form->isValid()) {
            //traiment de l'envoie dans le classe ContactNotification
            $notification->notifyAgentResponsable($contact);
            $this->addFlash("success", "Message envoyé, il vous repondra dans les brefs délais!");
            return $this->redirectToRoute('detail_bien', [
                'id' => $bien->getId(),
                'slugTitle' => $bien->getSlug()
            ]);
        }


        return $this->render('website/produit/detail.html.twig', [
            'slug' => 'detail_bien',
            'bien' => $bien,
            'similars' => $similar,
            'id' => $bien->getId(),
            'slugTitle' => $bien->getSlug(),
            'form' => $form->createView()
        ]);
    }
}
