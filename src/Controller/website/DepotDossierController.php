<?php declare(strict_types=1);

namespace App\Controller\website;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DepotDossierController extends AbstractController
{
    /**
     * @Route("/creer-un-dossier", name="ajouter-dossier")
     */
    public function ajouterDossier(): Response
    {
        return $this->render('website/dossier/ajouter.html.twig', [
            'slug' => 'ajouter-dossier-client'
        ]);
    }
    /**
     * @Route("/mes-dossiers", name="liste-dossiers")
     */
    public function listeDossiers(): Response
    {
        return $this->render('website/dossier/liste.html.twig', [
            'slug' => 'ajouter-dossiers-client'
        ]);
    }
    /**
     * @Route("/le-dossier", name="le-dossier")
     */
    public function afficherDossier(): Response
    {
        return $this->render('website/dossier/afficher.html.twig', [
            'slug' => 'afficher'
        ]);
    }
}
