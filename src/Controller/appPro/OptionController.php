<?php

namespace App\Controller\appPro;

use App\Entity\Option;
use App\Form\OptionType;
use App\Repository\OptionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/option")
 */
class OptionController extends AbstractController
{
    /**
     * @Route("/liste", name="creer_option_pro", methods={"GET","POST"})
     */
    public function creer(
        EntityManagerInterface $entityManager,
        Request $request,
        OptionRepository $optionRepository
    ): Response {
        $option = new Option();
        $form = $this->createForm(OptionType::class, $option);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($option);
            $entityManager->flush();
            $this->addFlash('success', "L'option a été crée avec succès' 👍");

            return $this->redirectToRoute('creer_option_pro');
        }

        return $this->render('appPro/option/creer_option.html.twig', [
            'options' => $optionRepository->findAll(),
            'slug' => 'creer_option_pro',
            'option' => $option,
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/editer/{id}", name="editer_option_pro", methods={"GET","POST"})
     */
    public function editer(
        EntityManagerInterface $entityManager,
        Request $request,
        Option $option
    ): Response {
        $form = $this->createForm(OptionType::class, $option);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            $this->addFlash('success', "L'option a été modifié avec succès' 👍");

            return $this->redirectToRoute('creer_option_pro');
        }

        return $this->render('appPro/option/editer_option.html.twig', [
            'slug' => 'creer_option_pro',
            'option' => $option,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/supprimer/{id}", name="supprimer_option_pro", methods={"GET"})
     */
    public function delete(EntityManagerInterface $entityManager, Option $option): Response
    {
        $entityManager->remove($option);
        $entityManager->flush();
        $this->addFlash('success', "L'option a été supprimé avec succès' 👍");


        return $this->redirectToRoute('creer_option_pro');
    }
}
