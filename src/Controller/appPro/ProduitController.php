<?php


namespace App\Controller\appPro;

use DateTime;
use App\Entity\Biens;
use App\Entity\Image;
use App\Entity\Agence;
use App\Form\BiensType;
use App\Entity\AdminSearchBien;
use App\Form\AdminSearchBienType;
use App\Repository\BiensRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/produit")
 */
class ProduitController extends AbstractController
{
    private $biensRepository;
    private $entityManager;

    public function __construct(BiensRepository $biensRepository, EntityManagerInterface $entityManager)
    {
        $this->biensRepository = $biensRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/liste", name="liste_produit_pro")
     * liste function qui affiche l'ensemble de tous les biens de l'application
     * et gérer la pagination
     *
     * @return Response
     */
    public function liste(PaginatorInterface $paginator, Request $request): Response
    {

        //Ajouter un formulaire de recherche à la liste de nos biens
        $rechercheBien = new AdminSearchBien();
        $form = $this->createForm(AdminSearchBienType::class, $rechercheBien);
        $form->handleRequest($request);


        $lengthBiens = $this->biensRepository->findAll();
        //Ajouter une pagination à la liste de nos biens et filtrer si possible
        $data = $this->biensRepository->getAllBiensforAdmin($rechercheBien);

        $biens = $paginator->paginate(
            $data, //On passe les données
            $request->query->getInt('page', 1), //Numéro de la pasge en cours, 1 par défaut
            10 //Nombre d'élement par page
        );
        // dd($lengthBiens);
        return $this->render('appPro/produit/liste.html.twig', [
            'slug' => 'liste_produit_pro',
            'biens' => $biens,
            'lengthBiens' => $lengthBiens,
            'form' => $form->createView()

        ]);
    }



    /**
     * @Route("/ajouter", name="ajouter_produit_pro")
     */
    public function ajouter(Request $request): Response
    {
        $bien = new Biens();
        $form = $this->createForm(BiensType::class, $bien);
        $form->handleRequest($request);
        // dd($form);
        if ($form->isSubmitted() && $form->isValid()) {
            $bien->setReferenceBien($bien->getSlug());
            $this->entityManager->persist($bien);
            $this->entityManager->flush();

            return $this->redirectToRoute('afficher_produit_pro', [
                'id' => $bien->getId(),
                'slugTitle' => $bien->getSlug()
            ]);
        }

        return $this->render('appPro/produit/ajouter.html.twig', [
            'slug' => 'ajouter_produit_pro',
            'bien' => $bien,
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/editer/{slugTitle}-{id}", name="editer_produit_pro", requirements={"slugTitle": "[a-z0-9\-]*"})
     */
    public function editer(Biens $bien, Request $request): Response
    {
        $form = $this->createForm(BiensType::class, $bien);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $bien->setReferenceBien($bien->getSlug());
            $bien->setUpdatedAt(new DateTime());
            $this->entityManager->persist($bien);
            $this->entityManager->flush();
            $this->addFlash('success', "Le produit a été mise à jour avec succès 👍");

            return $this->redirectToRoute('afficher_produit_pro', [
                'id' => $bien->getId(),
                'slugTitle' => $bien->getSlug()
            ]);
        }

        return $this->render('appPro/produit/editer.html.twig', [
            'slug' => 'editer_produit_pro',
            'form' => $form->createView(),
            'bien' => $bien,
            'id' => $bien->getId(),
            'slugTitle' => $bien->getSlug()

        ]);
    }



    /**
     * @Route("/afficher/{slugTitle}-{id}",  name="afficher_produit_pro", requirements={"slugTitle": "[a-z0-9\-]*"})
     */
    public function afficher(Biens $bien): Response
    {
        return $this->render('appPro/produit/afficher.html.twig', [
            'slug' => 'afficher_produit_pro',
            'bien' => $bien,
            'id' => $bien->getId(),
            'slugTitle' => $bien->getSlug()
        ]);
    }



    /**
     * @Route("/afficher/{slugTitle}-{id}",  name="supprimer_produit_pro")
     */
    public function supprimer(Biens $bien): Response
    {
        return $this->render('appPro/produit/afficher.html.twig', [
            'slug' => 'afficher_produit_pro',
            'bien' => $bien,
            'id' => $bien->getId(),
            'slugTitle' => $bien->getSlug()
        ]);
    }


    /**
     * @Route("/delete-image/{id}", name="supprimer_image_pro" )
     * Supprimer une ou plusieurs image (s) d'un bien
     */
    public function supprimerImage(
        Image $image,
        EntityManagerInterface $entityManager,
        CacheManager $cacheManager,
        UploaderHelper $helper
    ): Response {
        //On sauvegarde l'id et slug du bien pour rediriger l'utilisateur vers la page d'édition
        $bienId = $image->getBiens()->getId();
        $bienSlugTitle =  $image->getBiens()->getSlug();
        //Vidé l'image dans le cache avant la mise à jour
        if ($image->getImageFile() instanceof UploadedFile) {
            $cacheManager->remove($helper->asset($image, 'imageFile'));
        }
        $entityManager->remove($image);
        $entityManager->flush();
        $this->addFlash('success', "Image supprimée avec succès' 👍");


        return $this->redirectToRoute('editer_produit_pro', [
            'id' => $bienId,
            'slugTitle' => $bienSlugTitle
        ]);
    }
    /**
    * @Route("/supprimer-produit/{id}", name="supprimer_produit_pro", methods={"GET"})
    * @param Biens $bien
    * @param EntityManagerInterface $entityManager
    * @return Response
    */
    public function softDeleteArticle(Biens $bien, EntityManagerInterface $entityManager): Response
    {
        $bien->setDeletedAt(new DateTime());

        $entityManager->persist($bien);
        $entityManager->flush();
        $this->addFlash('success', "Larticle a bien été archivé 👍");

        return $this->redirectToRoute('liste_produit_pro');
    }
}
