<?php

namespace App\Controller\appPro;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/client")
 */
class ClientController extends AbstractController
{
    /**
     * @Route("/liste/", name="client_liste", methods={"GET"})
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function afficherClients(): Response
    {
        return $this->render('appPro/client/liste.html.twig', [
            'slug' => 'client_liste'
        ]);
    }
}
