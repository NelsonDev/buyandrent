<?php

namespace App\Controller\appPro;

use DateTime;
use App\Entity\Rendezvous;
use App\Form\RendezVousType;
use App\Repository\RendezvousRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/rendez-vous")
 */
class RendezVousClientController extends AbstractController
{

    /**
     * @Route("/ajouter", name="rdv_ajouter")
     * @param EntityManagerInterface $entityManager
     */
    public function rdv(Request $request, EntityManagerInterface $entityManager): Response
    {
        $rdv = new Rendezvous();
        $form = $this->createForm(RendezVousType::class, $rdv);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $rdv->setCreatedAt(new DateTime());
            $entityManager->persist($rdv);
            $entityManager->flush();
            $this->addFlash('success', "Votre rendez-vous à bien été ajouté");

            return $this->redirectToRoute('home_pro');
        }
        return $this->render('appPro/rdv/ajouter.html.twig', [
            'slug' => 'rdv_ajouter',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/liste", name="liste_rdv_pro")
     * liste function qui affiche l'ensemble de tous les rdv de l'agent
     * et gérer la pagination
     *
     * @return Response
     */
    public function liste(
        RendezvousRepository $rdvRepository,
        Request $request,
        EntityManagerInterface $entityManager
    ): Response {
        $rdvJanvier = $rdvRepository->getRdvJanvier();
        $rdvFevrier = $rdvRepository->getRdvFevrier();
        $rdvMars = $rdvRepository->getRdvMars();
        $rdvAvril = $rdvRepository->getRdvAvril();
        $rdvMai = $rdvRepository->getRdvMai();
        $rdvJuin = $rdvRepository->getRdvJuin();
        $rdvJuillet = $rdvRepository->getRdvJuillet();
        $rdvAout = $rdvRepository->getRdvAout();
        $rdvSeptembre = $rdvRepository->getRdvseptembre();
        $rdvOctobre = $rdvRepository->getRdvOctobre();
        $rdvNovembre = $rdvRepository->getRdvNovembre();
        $rdvDecembre = $rdvRepository->getRdvDecembre();

        $rdv = new Rendezvous();
        $form = $this->createForm(RendezVousType::class, $rdv);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $rdv->setCreatedAt(new DateTime());
            $entityManager->persist($rdv);
            $entityManager->flush();
            $this->addFlash('success', "Votre rendez-vous à bien été ajouté");

            return $this->redirectToRoute('liste_rdv_pro');
        }
        return $this->render('appPro/rdv/liste.html.twig', [
            'slug' => 'liste_rdv_pro',
            'form' => $form->createView(),
            'rdvListe' => $rdvRepository->findAll(),
            'rdvJanvier' => $rdvJanvier,
            'rdvFevrier' => $rdvFevrier,
            'rdvMars' => $rdvMars,
            'rdvAvril' => $rdvAvril,
            'rdvMai' => $rdvMai,
            'rdvJuin' => $rdvJuin,
            'rdvJuillet' => $rdvJuillet,
            'rdvAout' => $rdvAout,
            'rdvSeptembre' => $rdvSeptembre,
            'rdvOctobre' => $rdvOctobre,
            'rdvNovembre' => $rdvNovembre,
            'rdvDecembre' => $rdvDecembre,
        ]);
    }
    /**
     * @Route("/supprimer-rdv/{id}", name="delete_rdv", methods={"GET"})
     * @param Rendezvous $rendezvous
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function softDeleteArticle(Rendezvous $rendezvous, EntityManagerInterface $entityManager): Response
    {
        $rendezvous->setDeletedAt(new DateTime());

        $entityManager->persist($rendezvous);
        $entityManager->flush();
        $this->addFlash('success', "Le rendez-vous a bien été archivé");

        return $this->redirectToRoute('liste_rdv_pro');
    }
}
