<?php

namespace App\Controller\appPro;

use App\Entity\Biens;
use App\Entity\Agence;
use App\Repository\AgenceRepository;
use App\Services\GeneratePdfService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/fiche-vitrine")
 */
class FicheVitrineController extends AbstractController
{
    /**
     * @Route("/liste/", name="fiche_vitrine_liste", methods={"GET"})
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function afficherFicheVitrine(): Response
    {
        return $this->render('appPro/fiche-vitrine/liste.html.twig', [
            'slug' => 'fiche_vitrine_liste'

        ]);
    }
    /**
     * @Route("/creer-fiche-vitrine/{id}", name="fiche_vitrine_creer_m1", methods={"GET"})
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function creerFicheVitrine(Biens $bien, AgenceRepository $agenceRepository): Response
    {
        $agence = $agenceRepository->findAll();
        $id_bien = $bien->getId();
        //gestion de la facture
        $absPath = $this->getParameter('kernel.project_dir') . '/public/';
        $filename = $absPath . "uploads/pdf/fiche_vitrine" . $id_bien . ".pdf";
        $generatePdfService = new GeneratePdfService;
        $template = $this->renderView('appPro/fiche-vitrine/templates/model1.html.twig', [
            'bien' => $bien,
            'agence' => $agence
        ]);
        $generatePdfService->generatePdf($filename, $template);
        return $this->render('appPro/fiche-vitrine/creer.html.twig', [
            'slug' => 'fiche_vitrine_liste',
            'bien' => $bien,
            'agence' => $agence[0],
        ]);
    }
    /**
     * @Route("/creer-fiche-vitrine-model-2/{id}", name="fiche_vitrine_creer_m2", methods={"GET"})
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function creerFicheVitrineM2(Biens $bien, AgenceRepository $agenceRepository): Response
    {
        $agence = $agenceRepository->findAll();
        $id_bien = $bien->getId();
        //gestion de la facture
        $absPath = $this->getParameter('kernel.project_dir') . '/public/';
        $filename = $absPath . "uploads/pdf/fiche_vitrine" . $id_bien . ".pdf";
        $generatePdfService = new GeneratePdfService;
        $template = $this->renderView('appPro/fiche-vitrine/templates/model2.html.twig', [
            'bien' => $bien,
            'agence' => $agence
        ]);
        $generatePdfService->generatePdf($filename, $template);
        return $this->render('appPro/fiche-vitrine/creer_m2.html.twig', [
            'slug' => 'fiche_vitrine_liste',
            'bien' => $bien,
            'agence' => $agence[0],
        ]);
    }
    /**
     * @Route("/creer-fiche-vitrine-model-3/{id}", name="fiche_vitrine_creer_m3", methods={"GET"})
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function creerFicheVitrineM3(Biens $bien, AgenceRepository $agenceRepository): Response
    {
        $agence = $agenceRepository->findAll();
        $id_bien = $bien->getId();
        //gestion de la facture
        $absPath = $this->getParameter('kernel.project_dir') . '/public/';
        $filename = $absPath . "uploads/pdf/fiche_vitrine" . $id_bien . ".pdf";
        $generatePdfService = new GeneratePdfService;
        $template = $this->renderView('appPro/fiche-vitrine/templates/model3.html.twig', [
            'bien' => $bien,
            'agence' => $agence
        ]);
        $generatePdfService->generatePdf($filename, $template);
        return $this->render('appPro/fiche-vitrine/creer_m3.html.twig', [
            'slug' => 'fiche_vitrine_liste',
            'bien' => $bien,
            'agence' => $agence[0],
        ]);
    }
    /**
     * @Route("/creer-fiche-vitrine-model-4/{id}", name="fiche_vitrine_creer_m4", methods={"GET"})
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function creerFicheVitrineM4(Biens $bien, AgenceRepository $agenceRepository): Response
    {
        $agence = $agenceRepository->findAll();
        $id_bien = $bien->getId();
        //gestion de la facture
        $absPath = $this->getParameter('kernel.project_dir') . '/public/';
        $filename = $absPath . "uploads/pdf/fiche-vitrine-model4/fiche-vitrine-" . $id_bien . ".pdf";
        $generatePdfService = new GeneratePdfService;
        $template = $this->renderView('appPro/fiche-vitrine/templates/model4.html.twig', [
            'bien' => $bien,
            'agence' => $agence
        ]);
        $generatePdfService->generatePdf($filename, $template);
        return $this->render('appPro/fiche-vitrine/creer_m4.html.twig', [
            'slug' => 'fiche_vitrine_liste',
            'bien' => $bien,
            'agence' => $agence[0],
        ]);
    }
}
