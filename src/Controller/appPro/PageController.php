<?php

namespace App\Controller\appPro;

use DateTime;
use App\Entity\Note;
use App\Form\NoteType;
use App\Repository\NoteRepository;
use App\Repository\BiensRepository;
use App\Repository\AgenceRepository;
use App\Repository\RendezvousRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PageController extends AbstractController
{
    /**
     * @Route("/tableau-de-bord", name="home_pro")
     */
    public function index(
        RendezvousRepository $rdvRepository,
        AgenceRepository $agenceRepository,
        BiensRepository $biensRepository,
        Request $request,
        NoteRepository $noteRepository,
        EntityManagerInterface $em
    ): Response {
        //Récupérer la taille des biens par catégorie
        $total_maison = $biensRepository->getTotalMaison();
        $total_appart = $biensRepository->getTotalAppartement();
        $total_villa = $biensRepository->getTotalVilla();
        $total_bureau = $biensRepository->getTotalBureau();
        $total_studio = $biensRepository->getTotalStudio();
        //Récupérer tous les biens (vendus ou pas)
        $agence = $agenceRepository->findAll();

        //Récupérer les 4 derniers biens (vendus ou pas)
        $biens = $biensRepository->find4Lastproperties();

        // ajouter une note
        $note = new Note();
        $form = $this->createForm(NoteType::class, $note);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $note->setCreatedAt(new DateTime());
            $em->persist($note);
            $em->flush();
            $this->addFlash('success', "Note ajouté avec succès");

            return $this->redirectToRoute('home_pro');
        }
        return $this->render('appPro/index.html.twig', [
            'slug' => 'home_pro',
            'agence' => $agence[0],
            'biens' => $biens,
            'total_maison' => $total_maison,
            'total_appart' => $total_appart,
            'total_villa' => $total_villa,
            'total_bureau' => $total_bureau,
            'total_studio' => $total_studio,
            'lastRdvListe' => $rdvRepository->findLastRdv(),
            'findLastNote' => $noteRepository->findLastNote(),
            'form' => $form->createView(),
        ]);
    }
}
