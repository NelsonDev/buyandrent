<?php

namespace App\Controller\appPro;

use App\Entity\Agence;
use App\Form\AgenceCouleurType;
use App\Repository\AgenceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/configuration")
 * @param Agence $agence
 * @return Response
 */
class ParamettreController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/interface/{id}", name="param_interface_pro")
     * @param Agence $agence
     * @return Response
     */
    public function modifierCouleur(Request $request, Agence $agence, AgenceRepository $agenceRepository): Response
    {
        $dataAgence = $agenceRepository->findAll();
        $couleur1 = $agenceRepository->findByCoueleur1();
        $couleur2 = $agenceRepository->findByCoueleur2();
        $form = $this->createForm(AgenceCouleurType::class, $agence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($agence);
            $this->entityManager->flush();
            $this->addFlash('success', "Paramettre agence mis à jour");
            return $this->redirectToRoute('param_interface_pro', ['id' => $agence->getId()]);
        }
        return $this->render('appPro/paramettre/interface.html.twig', [
            'slug' => 'param_interface_pro',
            'form' => $form->createView(),
            'couleur1' => $couleur1,
            'couleur2' => $couleur2,
            'agence' => $agence,
            'dataAgence' => $dataAgence
        ]);
    }
    /**
     * @Route("/admin/info", name="admin_detail_agence")
     */
    public function detailAgence(AgenceRepository $agenceRepository): Response
    {
        $agence = $agenceRepository->findAll();
        // dd($agenceRepository->findAll());
        return $this->render('appPro/paramettre/interface.html.twig', [

            'slug' => 'info_agence',
            'agence' => $agence[0],
        ]);
    }
     /**
     * @Route("/agent/ajouter", name="admin_ajouter_agent")
     */
    public function ajouterCompte(): Response
    {
        return $this->render('appPro/paramettre/agents.html.twig', [
            'slug' => 'info_agence',
        ]);
    }
}
