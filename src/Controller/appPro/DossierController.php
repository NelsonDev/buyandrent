<?php

namespace App\Controller\appPro;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/dossier")
 */
class DossierController extends AbstractController
{
    /**
     * @Route("/liste/", name="dossier_liste_pro", methods={"GET"})
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function listedossiers(): Response
    {
        return $this->render('appPro/dossier/liste.html.twig', [
            'slug' => 'dossier_liste_pro'

        ]);
    }
    /**
     * @Route("/afficher/", name="dossier_afficher_pro", methods={"GET"})
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function afficherdossiers(): Response
    {
        return $this->render('appPro/dossier/afficher.html.twig', [
            'slug' => 'dossier_afficher_pro'

        ]);
    }
}
