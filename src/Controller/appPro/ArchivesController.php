<?php

declare(strict_types=1);

namespace App\Controller\appPro;

use DateTime;
use App\Entity\Biens;
use App\Entity\Agence;
use App\Form\AgenceType;
use App\Repository\BiensRepository;
use App\Repository\AgenceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use MercurySeries\FlashyBundle\FlashyNotifier;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use phpDocumentor\Reflection\Types\Null_;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ArchivesController extends AbstractController
{
    /**
     * @Route("/admin/archives", name="archives")
     */
    public function archiveBien(
        BiensRepository $biensRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $lengthBiens = $biensRepository->biensArchives();
        //Ajouter une pagination à la liste de nos biens et filtrer si possible
        $data = $biensRepository->biensArchives();

        $biens = $paginator->paginate(
            $data, //On passe les données
            $request->query->getInt('page', 1), //Numéro de la pasge en cours, 1 par défaut
            10 //Nombre d'élement par page
        );


        return $this->render('appPro/archives.html.twig', [
            'slug' => 'info_agence',
            'biens' => $biens,
            'lengthBiens' => $lengthBiens,
        ]);
    }

    /**
     * @Route("/admin/archives/supprimer-bien/{id}", name="supprimer_bien", methods={"GET"})
     * @param Biens $bien
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function supprimierBien(Biens $bien, EntityManagerInterface $entityManager): Response
    {
        $entityManager->remove($bien);
        $entityManager->flush();
        $this->addFlash('success', "La catégorie a bien été définitivement supprimée de la base de données");

        return $this->redirectToRoute('archives');
    }
    /**
     * @Route("/restaurer-produit/{id}", name="restaurer_produit_pro", methods={"GET"})
     * @param Biens $bien
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function softDeleteArticle(Biens $bien, EntityManagerInterface $entityManager): Response
    {
        $bien->setDeletedAt(null);

        $entityManager->persist($bien);
        $entityManager->flush();
        $this->addFlash('success', "Bien restauré avec succès 👍");

        return $this->redirectToRoute('liste_produit_pro');
    }
}
