<?php

namespace App\Controller\appPro;

use DateTime;
use App\Repository\BiensRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin")
 */
class StatistiqueController extends AbstractController
{

    /**
     * @Route("/statistiques", name="statistique")
     * @param EntityManagerInterface $entityManager
     */
    public function statistiques(BiensRepository $biensRepository, Request $request): Response
    {
        $maisonVendu = $biensRepository->totalMaisonVendu();
        $appartVendu = $biensRepository->totalAppartVendu();
        $bureauVendu = $biensRepository->totalBureauVendu();
        $studioVendu = $biensRepository->totalStudioVendu();
        $villaVendu = $biensRepository->totalVillaVendu();
        $totalBiensVendus = $biensRepository->totalBiensVendus();
        return $this->render('appPro/statistiques/statistique.html.twig', [
            'slug' => 'statistique',
            'maisonVendu' => $maisonVendu,
            'appartVendu' => $appartVendu,
            'bureauVendu' => $bureauVendu,
            'studioVendu' => $studioVendu,
            'villaVendu' => $villaVendu,
            'totalBiensVendus' => $totalBiensVendus,

        ]);
    }
}
