<?php


namespace App\Controller\appPro;

use DateTime;
use App\Entity\Agence;
use App\Form\AgenceType;
use App\Repository\AgenceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AgenceController extends AbstractController
{
    private $agenceRepository;
    private $entityManager;

    public function __construct(
        AgenceRepository $agenceRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->agenceRepository = $agenceRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/admin/info", name="info_agence")
     *
     * @param Agence $agence
     * @return Response
     */
    public function showAgence(): Response
    {
        $agence = $this->agenceRepository->findAll();
        return $this->render('appPro/agence/show_agence.html.twig', [

            'slug' => 'info_agence',
            'agence' => $agence[0],
        ]);
    }


    /**
     * @Route("/admin/edit-agence-{id}", name="edit_agence", methods={"GET|POST"})
     *
     * editerAgence (Editer les informations de l'agence)
     * @param Agence $agence
     * @param Request $request
     * @return Response
     */
    public function editerAgence(
        Agence $agence,
        Request $request,
        CacheManager $cacheManager,
        UploaderHelper $helper
    ): Response {
        $form = $this->createForm(AgenceType::class, $agence);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            //Vidé l'image dans le cache avant la mise à jour
            if ($agence->getImageFile() instanceof UploadedFile) {
                $cacheManager->remove($helper->asset($agence, 'imageFile'));
            }
            $agence->setUpdatedAt(new DateTime());
            $this->entityManager->flush();
            $this->addFlash('success', "Le produit à bien été mis à jour");

            return $this->redirectToRoute('info_agence');
        }

        return $this->render('appPro/agence/edit_agence.html.twig', [

            'slug' => 'edit_agence',
            'agence' => $agence,
            'form' => $form->createView(),
        ]);
    }
}
