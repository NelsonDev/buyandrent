<?php

namespace App\Controller\appClient;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/dossier")
 */
class DossierController extends AbstractController
{
    /**
     * @Route("/deposer/", name="deposer_dossier_client", methods={"GET"})
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function listedossiers(): Response
    {
        return $this->render('appPro/dossier/deposer.html.twig', [
            'slug' => 'deposer_dossier_client'

        ]);
    }
}
