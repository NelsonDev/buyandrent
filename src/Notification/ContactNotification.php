<?php

namespace App\Notification;

use Twig\Environment;
use App\Entity\Contact;

class ContactNotification
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var Environment
     */
    private $renderer;


    public function __construct(\Swift_Mailer $mailer, Environment $renderer)
    {
        $this->mailer = $mailer;
        $this->renderer = $renderer;
    }


    /**
     * notifyAgentResponsable
     * notifyAgence (fonction permettant à  un une personne non connecter
     * d'envoyer un email à l'agent responsable du bien publié)
     * @param  mixed $contact
     * @return void
     */
    public function notifyAgentResponsable(Contact $contact)
    {
        $message = (new \Swift_Message('Buy And Rent : ' . $contact->getBien()->getTitre()))
            ->setFrom($contact->getEmail())
            ->setTo($contact->getBien()->getUsers()->getEmail())
            ->setReplyTo($contact->getEmail())
            ->setBody(
                $this->renderer->render('website/emails/contactBien.html.twig', [
                    'contact' => $contact
                ]),
                'text/html'
            );
        $this->mailer->send($message);
    }



    /**
     * notifyAgence (fonction permettant à  un une personne non connecter
     * d'envoyer un email à l'agence)
     *
     * @param  mixed $contact
     * @return void
     */
    public function notifyAgence(Contact $contact)
    {
        $message = (new \Swift_Message("Un message adressé à l'agence"))
            ->setFrom($contact->getEmail())
            ->setTo('buyandrent.service@gmail.com')
            ->setReplyTo($contact->getEmail())
            ->setBody(
                $this->renderer->render('website/emails/contactAgence.html.twig', [
                    'contact' => $contact
                ]),
                'text/html'
            );
        $this->mailer->send($message);
    }
}
