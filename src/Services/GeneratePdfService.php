<?php

declare(strict_types=1);

namespace App\Services;

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;

class GeneratePdfService
{
    public function generatePdf($filename, $template)
    {
        try {
            $html2pdf = new Html2Pdf('P', 'A4', 'fr', true, 'UTF-8');
            $html2pdf->writeHTML($template);
            $path = $filename;
            $html2pdf->output($path, 'F');
        } catch (Html2PdfException $e) {
            die($e);
        }
    }
}
