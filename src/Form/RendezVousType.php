<?php

namespace App\Form;

use App\Entity\Users;
use App\Entity\Rendezvous;
use App\Repository\UsersRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class RendezVousType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('type', ChoiceType::class, [
                'label' => false,
                'choices' => [
                    'Rendez-vous client' => 'Rdv',
                    'Visite' => 'Visite',
                    'Etat des lieux' => 'EdL',
                    'Autre' => 'Autre',
                ],
                'placeholder' => '--',
            ])
            ->add('titre', TextType::class, [
                'label' => false,
            ])
            ->add('contactNom', TextType::class, [
                'label' => false,
            ])
            ->add('description', TextareaType::class, [
                'label' => false,
                'attr' => array('cols' => '5', 'rows' => '10'),
            ])

            ->add('dateVisite', DateType::class, [
                'label' => false,
                'required' => true,
                'widget' => "single_text",

            ])
            ->add('heureVisite', TimeType::class, [
                'label' => false,
                'widget' => 'single_text',
            ])

            ->add('user', EntityType::class, [
                'class' => Users::class,
                'label' => false,
                'required' => true,
                'query_builder' => function (UsersRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.nom', 'ASC');
                },
                'choice_label' => 'nom',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Rendezvous::class,
        ]);
    }
}
