<?php

namespace App\Form;

use App\Entity\Biens;
use App\Entity\Option;
use App\Entity\Categories;
use App\Entity\AdminSearchBien;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class AdminSearchBienType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            //Préciser si possible la surface minimale du bien ou des biens à rechercher
            ->add('surfaceMin', IntegerType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Surface minimale',
                ]

            ])
            //Préciser si possible la surface maximale du bien ou des biens à rechercher
            ->add('surfaceMax', IntegerType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Surface maximale',
                ]

            ])
            //Préciser si possible le prix maximal
            ->add('prixMax', IntegerType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Budget max'
                ]
            ])
            //Préciser si possible le prix minimal
            ->add('prixMin', IntegerType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Budget min'
                ]
            ])
            //Préciser si possible le nombre de pièces
            ->add('nbrepiece', IntegerType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Nombre de pièces'
                ]
            ])
            //Préciser si possible le nombre de chambres
            ->add('nbreChambre', IntegerType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Nombre de chambres'
                ]
            ])
            //ajout du champ permettant de d'ajouter des options dans notre recherche
            ->add('options', EntityType::class, [
                'required' => false,
                'label' => false,
                'class' => Option::class,
                'choice_label' => 'nom',
                'multiple' => true,
                'attr' => [
                    'placeholder' => 'options',
                ]
            ])
            //Rechercher les biens en fonction de l'adresse
            ->add('adresse', null, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Adresse',
                ]
            ])
            //Rechercher les biens en fonction du type de transaction (Vente ou Location)
            ->add('typeTransaction', ChoiceType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Type de transaction',
                ],
                'choices' => $this->getTypeTransaction(),

            ])
            //Rechercher les biens en fonction du type de chauffage
            ->add('typeChauffage', ChoiceType::class, [
                'label' => false,
                'required' => false,
                'choices' => $this->getTypeChauffage(),
              
            ])
            //Rechercher les biens en fonction d'une distance
            ->add('distance', ChoiceType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Rayon',
                ],
                'choices' => [
                    '10 km' => 10,
                    '50 km' => 50,
                    '100 km' => 100,
                    '150 km' => 150,
                    '250 km' => 250,
                    '300 km' => 300,
                    '400 km' => 400,
                    '500 km' => 500,
                    '1000 km' => 1000,
                    'Plus de 1000 km' => 600000
                ]
            ])
            //Récupérer les coordonnées géographiques de l'adresse pas besoin de les afficher
            ->add('latitude', HiddenType::class)
            ->add('longitude', HiddenType::class)
            //Rechercher les biens en fonction de la catégorie
            ->add('categorie', EntityType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Categorie',
                ],
                'class' => Categories::class,
                'choice_label' => 'nom',
                'multiple' => false
            ]);
    }

    /**
     * Undocumented function
     *
     * @param OptionsResolver $resolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AdminSearchBien::class,
            // ajout de la méthode en GET et désactiver le csrf_protection car pas besoin de token pour la recherche
            'method' => 'GET',
            'csrf_protection' => false
        ]);
    }

    /**
     * getTypeTransaction permet de récupérer les valeurs de notre constance TRANSACTION (Location ou Vente)
     *
     * @return array
     */
    private function getTypeTransaction(): array
    {
        $typeTransaction = Biens::TRANSACTION;
        $output = [];
        foreach ($typeTransaction as $key => $value) {
            $output[$value] = $key;
        }
        return $output;
    }

    /**
     * getChauffage permet de récupérer les valeurs de notre constance CHAUFFAGE
     *
     * @return array
     */
    private function getTypeChauffage(): array
    {
        $typeChauffage = Biens::CHAUFFAGE;
        $output = [];
        foreach ($typeChauffage as $key => $value) {
            $output[$value] = $key;
        }
        return $output;
    }

    /**
     * getBlockPrefix permet de donner des paramètres un peu plus propre dans url après une recherche
     *
     * @return void
     */
    public function getBlockPrefix()
    {
        return '';
    }
}
