<?php

namespace App\Form;

use App\Entity\Biens;
use App\Entity\Users;
use App\Entity\Option;
use App\Entity\Categories;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class BiensType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('titre', TextType::class, [
                'required' => true,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Titre du bien',
                ],
            ])
            ->add('description', TextareaType::class, [
                'required' => true,
                'label' => false,
                'attr' => [
                    'placeholser' => "Descritpion courte du bien",
                    'rows' => '10',
                    'cols' => '10'
                ]
            ])
            ->add('surface', TextType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'Surface',
                ],
            ])
            ->add('nbrepiece', TextType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'Nombre de pièce',
                ],
            ])
            ->add('nbreChambre', TextType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'Nombre de chambre',
                ],
            ])
            ->add('etage', NumberType::class, [
                'label' => false,
                'attr' => ['placeholder' => 'Etage']
            ])
            ->add('typeChauffage', ChoiceType::class, [
                'label' => false,
                'choices' => $this->getTypeChauffage(),
            ])
            ->add('statusBien', CheckboxType::class, [
                'label' => false,
            ])
            ->add('proprioPrenom', TextType::class, [
                'label' => false,
                'attr' => ['placeholder' => 'Prénom']
            ])
            ->add('proprioNom', TextType::class, [
                'label' => false,
                'attr' => ['placeholder' => 'Nom']
            ])
            ->add('proprioNumero')
            ->add('proprioEmail', EmailType::class, [
                'label' => false,
                'attr' => ['placeholder' => 'Email']
            ])
            ->add('prix', NumberType::class, [
                'label' => false,
                'attr' => ['placeholder' => 'Prix du bien en euro']
            ])
            ->add('ville', TextType::class, [
                'label' => false,
                'attr' => ['placeholder' => 'Ville']
            ])
            ->add('adresse', TextType::class, [
                'label' => false,
                'attr' => ['placeholder' => 'Adresse']
            ])
            ->add('codePostal', TextType::class, [
                'label' => false,
                'attr' => ['placeholder' => 'Code postal']
            ])
            ->add('referenceBien', TextType::class, [
                'label' => false,
                'attr' => ['disabled' => 'disabled']
            ])
            ->add('chargeComprise', CheckboxType::class, [
                'label' => false,
            ])
            ->add('typeTransaction', ChoiceType::class, [
                'label' => false,
                'choices' => $this->getTypeTransaction()
            ])
            ->add('categorie', EntityType::class, [
                'label' => false,
                'class' => Categories::class,
                'choice_label' => 'nom',
                'expanded' => true,
            ])
            ->add('users', EntityType::class, [
                'label' => false,
                'class' => Users::class,
                'choice_label' => 'prenom'
            ])
            ->add('options', EntityType::class, [
                'label' => false,
                'class' => Option::class,
                'required' => false,
                'choice_label' => 'nom',
                'multiple' => true,
                'attr' => [
                    'placeholder' => 'Ajouter des options au produit',
                    'class' => 'bg-warning'
                ]
            ])
            ->add('imageFiles', FileType::class, [
                'required' => false,
                'multiple' => true
            ])
            ->add('latitude')
            ->add('longitude');
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Biens::class,
            //pour traduire les champs de nos labels du formulaire
            'translation_domain' => 'formsBien'
        ]);
    }


    /**
     * getChauffage permet de récupérer les valeurs de notre constance CHAUFFAGE
     *
     * @return array
     */
    private function getTypeChauffage(): array
    {
        $typeChauffage = Biens::CHAUFFAGE;
        $output = [];
        foreach ($typeChauffage as $key => $value) {
            $output[$value] = $key;
        }
        return $output;
    }


    /**
     * getTypeTransaction permet de récupérer les valeurs de notre constance TRANSACTION
     *
     * @return array
     */
    private function getTypeTransaction(): array
    {
        $typeTransaction = Biens::TRANSACTION;
        $output = [];
        foreach ($typeTransaction as $key => $value) {
            $output[$value] = $key;
        }
        return $output;
    }
}
