<?php

namespace App\Form;

use App\Entity\Agence;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;

class AgenceCouleurType extends AbstractType
{
    //Les validateurs et les contraintes sont directement dans l'entité
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('couleur_1', TextType::class, [
                'label' => false,
                'required'   => false,
                // 'empty_data' => 'John Doe',
            ])
            ->add('couleur_2', ColorType::class, [
                'label' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Agence::class,
            //pour traduire les champs de nos labels du formulaire
            'translation_domain' => 'formsAgence'
        ]);
    }
}
