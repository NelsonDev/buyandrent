<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Biens;
use App\Entity\Users;
use App\Entity\Agence;
use App\Entity\Categories;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class BuyAndRentFixtures extends Fixture
{
    private $hasher;

    public function __construct(UserPasswordHasherInterface  $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {

        //Les catégories des biens de l'application qui seront associées à un bien
        $categories = ['Maison', 'Appartement', 'Studio', 'Villa', 'Bureau'];

        //Numréro aléatoire pour les utilisateurs.
        $number1 = '0654859874';
        $number2 = '0145215086';
        $number3 = '0695785410';
        $number4 = '0689527594';
        $number5 = '0752514871';
        $number6 = '0420514852';

        //Utilisation de Faker pour avoir des données un peu plus parlant en francais.
        $faker = Factory::create('fr_FR');

        //Créer l'entreprise Buy And Rend
        $agence = new Agence();
        $agence->setDenomination('Buy And Rent service')
            ->setDescription($faker->sentences(3, true))
            ->setEmail('buyandrend.service@gmail.com')
            ->setTelephone($number4)
            ->setAdresse($faker->address)
            ->setCodePostal($faker->postcode)
            ->setVille($faker->city);

        $manager->persist($agence);


        //Créer un premier utilisateur (Nelson)
        $user1 = new Users();
        $user1->setPrenom('Nelson')
            ->setNom('TAPINFO')
            ->setEmail('tapinfoulrichnelson@yahoo.com')
            ->setTelephone($faker->randomElement([$number1, $number2, $number3, $number4, $number5, $number6]));

        $password = $this->hasher->hashPassword($user1, 'BuyAndRent2022NYF');
        $user1->setPassword($password);

        $manager->persist($user1);


        //Créer second utilisateur (Yvanna)
        $user2 = new Users();
        $user2->setPrenom('Yvanna')
            ->setNom('RAZA')
            ->setEmail('razafiemilie@hotmail.fr')
            ->setTelephone($faker->randomElement([$number1, $number2, $number3, $number4, $number5, $number6]));

        $password = $this->hasher->hashPassword($user2, 'BuyAndRent2022NYF');
        $user2->setPassword($password);

        $manager->persist($user2);


        //Créer troisième utilisateur (Fatima)
        $user3 = new Users();
        $user3->setPrenom('Fatima')
            ->setNom('ELHASBI')
            ->setEmail('fatimazahra.elhasbi@gmail.com')
            ->setTelephone($faker->randomElement([$number1, $number2, $number3, $number4, $number5, $number6]));

        $password = $this->hasher->hashPassword($user3, 'BuyAndRent2022NYF');
        $user3->setPassword($password);

        $manager->persist($user3);

        //Créer les catégories de bien pour l'application
        for ($j = 0; $j < 5; $j++) {
            $categorie = new Categories();

            $categorie->setNom($categories[$j]);

            $manager->persist($categorie);

            //Créer entre 15 et 20 biens pour chaque catégorie créee
            for ($k = 1; $k < mt_rand(15, 20); $k++) {
                $bien  = new Biens();

                $bien->setTitre($faker->words(3, true))
                    ->setDescription($faker->sentences(3, true))
                    ->setSurface($faker->numberBetween(20, 500))
                    ->setNbrepiece($faker->numberBetween(2, 10))
                    ->setNbreChambre($faker->numberBetween(1, 10))
                    ->setEtage($faker->numberBetween(0, 15))
                    ->setTypeChauffage($faker->numberBetween(0, count(Biens::CHAUFFAGE) - 1))
                    ->setStatusBien($faker->randomElement([true, false]))
                    ->setProprioPrenom($faker->firstName())
                    ->setProprioNom($faker->lastName())
                    ->setProprioNumero($faker->randomElement([
                        $number1, $number2,
                        $number3, $number4,
                        $number5, $number6
                    ]))
                    ->setProprioEmail($faker->email)
                    ->setPrix($faker->numberBetween(2000, 500000))
                    ->setVille($faker->city)
                    ->setAdresse($faker->address)
                    ->setCodePostal($faker->postcode)
                    ->setLatitude(0)
                    ->setLongitude(0)
                    ->setReferenceBien($faker->uuid)
                    ->setCategorie($categorie)
                    ->setUsers($faker->randomElement([$user1, $user2, $user3]))
                    ->setChargeComprise($faker->randomElement([true, false]))
                    ->setTypeTransaction($faker->numberBetween(0, count(Biens::TRANSACTION) - 1));

                $manager->persist($bien);
            }
        }

        $manager->flush();
    }
}
