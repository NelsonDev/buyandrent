<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Rendezvous;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Rendezvous|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rendezvous|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rendezvous[]    findAll()
 * @method Rendezvous[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RendezvousRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Rendezvous::class);
    }

    // /**
    //  * @return Rendezvous[] Returns an array of Rendezvous objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Rendezvous
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findLastRdv()
    {
        return $this->createQueryBuilder('a')
            ->setMaxResults(3)
            ->addOrderBy('a.dateVisite', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function getRdvJanvier()
    {
        return $this->createQueryBuilder('c')
            ->andWhere('SUBSTRING(c.dateVisite,6,2) = 01')
            ->getQuery()
            ->getResult();
    }
    public function getRdvFevrier()
    {
        return $this->createQueryBuilder('d')
            ->andWhere('SUBSTRING(d.dateVisite,6,2) = 02')
            ->getQuery()
            ->getResult();
    }
    public function getRdvMars()
    {
        return $this->createQueryBuilder('e')
            ->andWhere('SUBSTRING(e.dateVisite,6,2) = 03')
            ->getQuery()
            ->getResult();
    }
    public function getRdvAvril()
    {
        return $this->createQueryBuilder('f')
            ->andWhere('SUBSTRING(f.dateVisite,6,2) = 04')
            ->getQuery()
            ->getResult();
    }
    public function getRdvMai()
    {
        return $this->createQueryBuilder('g')
            ->andWhere('SUBSTRING(g.dateVisite,6,2) = 05')
            ->getQuery()
            ->getResult();
    }
    public function getRdvJuin()
    {
        return $this->createQueryBuilder('h')
            ->andWhere('SUBSTRING(h.dateVisite,6,2) = 06')
            ->getQuery()
            ->getResult();
    }
    public function getRdvJuillet()
    {
        return $this->createQueryBuilder('i')
            ->andWhere('SUBSTRING(i.dateVisite,6,2) = 07')
            ->getQuery()
            ->getResult();
    }
    public function getRdvAout()
    {
        return $this->createQueryBuilder('j')
            ->andWhere('SUBSTRING(j.dateVisite,6,2) = 08')
            ->getQuery()
            ->getResult();
    }
    public function getRdvseptembre()
    {
        return $this->createQueryBuilder('k')
            ->andWhere('SUBSTRING(k.dateVisite,6,2) = 09')
            ->getQuery()
            ->getResult();
    }
    public function getRdvOctobre()
    {
        return $this->createQueryBuilder('l')
            ->andWhere('SUBSTRING(l.dateVisite,6,2) = 10')
            ->getQuery()
            ->getResult();
    }
    public function getRdvNovembre()
    {
        return $this->createQueryBuilder('n')
            ->andWhere('SUBSTRING(n.dateVisite,6,2) = 11')
            ->getQuery()
            ->getResult();
    }
    public function getRdvDecembre()
    {
        return $this->createQueryBuilder('o')
            ->andWhere('SUBSTRING(o.dateVisite,6,2) = 12')
            ->getQuery()
            ->getResult();
    }
}
