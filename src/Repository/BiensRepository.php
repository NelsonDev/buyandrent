<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Biens;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use App\Entity\AdminSearchBien;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Biens|null find($id, $lockMode = null, $lockVersion = null)
 * @method Biens|null findOneBy(array $criteria, array $orderBy = null)
 * @method Biens[]    findAll()
 * @method Biens[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 */
class BiensRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Biens::class);
    }


    /**
     * Répérer en fonction de la recherche les biens dans la partie pro de l'application
     * Cette recherche est fonction des informations passer dans le formulaire
     * @param AdminSearchBien $rechercheBien
     * @return Query
     */
    public function getAllBiensforAdmin(AdminSearchBien $rechercheBien): Query
    {

        //Sauvegarder ma requête dans une variable query
        $query =  $this->findVisibleQueryAdmin();

        //Si j'ai un prix minimal alors j'ajoute une condition pour setter le prix min
        if ($rechercheBien->getPrixMin()) {
            $query = $query
                ->andwhere('b.prix >= :prixMin')
                ->setParameter('prixMin', $rechercheBien->getPrixMin());
        }
        //Si j'ai un prix maximal alors j'ajoute une condition pour setter le prix max
        if ($rechercheBien->getPrixMax()) {
            $query = $query
                ->andwhere('b.prix <= :prixMax')
                ->setParameter('prixMax', $rechercheBien->getPrixMax());
        }

        //Si j'ai une surface minimale alors j'ajoute une condition pour setter la surface
        if ($rechercheBien->getSurfaceMin()) {
            $query = $query
                ->andwhere('b.surface >= :surfaceMin')
                ->setParameter('surfaceMin', $rechercheBien->getSurfaceMin());
        }
        //Si j'ai une surface maximale alors j'ajoute une condition pour setter la surface
        if ($rechercheBien->getSurfaceMax()) {
            $query = $query
                ->andwhere('b.surface <= :surfaceMax')
                ->setParameter('surfaceMax', $rechercheBien->getSurfaceMax());
        }

        //Si j'ai un nombre de pièces alors j'ajoute une condition pour setter le nombre de pièces
        if ($rechercheBien->getNbrepiece()) {
            $query = $query
                ->andwhere('b.nbrepiece = :nbrepiece')
                ->setParameter('nbrepiece', $rechercheBien->getNbrepiece());
        }
        //Si j'ai un nombre de chambres alors j'ajoute une condition pour setter le nombre de chambres
        if ($rechercheBien->getNbreChambre()) {
            $query = $query
                ->andwhere('b.nbreChambre = :nbreChambre')
                ->setParameter('nbreChambre', $rechercheBien->getNbreChambre());
        }


        //permet de récupérer les biens en fonction de la catégorie.
        if ($rechercheBien->getCategorie()) {
            $query = $query
                ->andwhere('b.categorie = :categorie')
                ->setParameter('categorie', $rechercheBien->getCategorie());
        }

        //permet de récupérer les biens en fonction du type de transaction (Vente ou Location).
        if ($rechercheBien->getTypeTransaction()) {
            $query = $query
                ->andwhere('b.typeTransaction = :typeTransaction')
                ->setParameter('typeTransaction', $rechercheBien->getTypeTransaction());
        }

        if ($rechercheBien->getTypeChauffage()) {
            $query = $query
                ->andwhere('b.typeChauffage = :typeChauffage')
                ->setParameter('typeChauffage', $rechercheBien->getTypeChauffage());
        }

        //permet de récupérer les biens en fonction de l'option demandée.
        if ($rechercheBien->getOptions()->count() > 0) {
            $k = 0;
            foreach ($rechercheBien->getOptions() as $option) {
                $k++;
                $query = $query
                    ->andwhere(":option$k MEMBER OF b.options")
                    ->setParameter("option$k", $option);
            }
        }
        //condition pour ce qui est du calcul de la longitude et la latitude
        if ($rechercheBien->getLatitude() && $rechercheBien->getLongitude() && $rechercheBien->getDistance()) {
            $query = $query
                ->select('b')
                ->andWhere('(6353 * 2 * ASIN(SQRT( POWER(SIN((b.latitude - :latitude) *  pi()/180 / 2), 2) 
                +COS(b.latitude * pi()/180) * COS(:latitude * pi()/180)
                 * POWER(SIN((b.longitude - :longitude) * pi()/180 / 2), 2) ))) <= :distance')
                ->setParameter('longitude', $rechercheBien->getLongitude())
                ->setParameter('latitude', $rechercheBien->getLatitude())
                ->setParameter('distance', $rechercheBien->getDistance());
        }


        return $query->getQuery();
    }

    /**
     * findAllVsisibleQueryAdmin pour factoriser le code
     * (récupère tous les biens vendu ou non dans la partie pro)
     *
     * @return QueryBuilder
     */
    private function findVisibleQueryAdmin(): QueryBuilder
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.deletedAt IS NULL');
    }

    /**
     * findLastproperties recherche les derniers biens ( les 8 derniers
     * biens publiés pour afficher à
     * page d'acceuil de l'application et qui sont non vendus ou loués)
     *
     * @return Biens[]
     */
    public function findLastproperties(): array
    {
        return $this->findVisibleQuery()
            ->setMaxResults(8)
            ->getQuery()
            ->getResult();
    }

    /**
     * biensSupprimes recherche les derniers biens archivés
     *
     * @return Biens[]
     */
    public function biensArchives(): QueryBuilder
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.deletedAt IS NOT NULL');
    }

    /**
     * findLastproperties recherche les derniers biens
     * ( les 4 derniers biens publiés pour afficher au
     * tableau de bord qui peuvent être vendu ou pas)
     *
     * @return Biens[]
     */
    public function find4Lastproperties(): array
    {
        return $this->createQueryBuilder('b')
            ->setMaxResults(4)
            ->getQuery()
            ->getResult();
    }


    /**
     * findAllVsisibleQuery pour factoriser le code (récupère tous les biens dont le
     * statusBien est à false, c'est à dire non vendu ou loué dans la partie client)
     *
     * @return QueryBuilder
     */
    private function findVisibleQuery(): QueryBuilder
    {
        return $this->createQueryBuilder('b')
            ->where('b.statusBien = false')
            ->andwhere('b.deletedAt IS NULL');
    }

    /**
     * findBySimilar recherche les 4 biens similaires
     */
    public function findBySimilar(array $datas, $id, $limit)
    {
        $query = $this->findVisibleQuery();

        foreach ($datas as $key => $value) {
            if ($value) {
                $query = $query
                    ->andWhere('b.' . $key . ' = :' . $key)
                    ->setParameter($key, $value);
            }
        }

        if ($id) {
            $query = $query
                ->andWhere('b.id != :id')
                ->setParameter('id', $id);
        }

        $query = $query
            ->orderBy('b.updatedAt', 'DESC');

        $query = $query
            ->setMaxResults($limit);


        return $query
            ->getQuery()
            ->getResult();
    }

    /**
     * findAllBienaVendre recherche tous les biens avec le statut à Louer
     *
     * @return QueryBuilder
     */
    public function findAllBienaLouer(): QueryBuilder
    {
        return $this->createQueryBuilder('b')
            ->where('b.statusBien = false')
            ->andwhere('b.deletedAt IS NULL')
            ->andwhere('b.typeTransaction=0');
        // ->getQuery()
        // ->getResult();
    }

    /**
     * Répérer en fonction de la recherche les biens dans la partie web de l'application
     * Cette recherche est fonction des informations passer dans le formulaire
     * @param AdminSearchBien $rechercheBien
     * @return Query
     */
    public function findAllBienaLouerFormWebSite(AdminSearchBien $rechercheBien): Query
    {
        //Sauvegarder ma requête dans une variable query pour ajouter des conditions
        $query =  $this->findAllBienaLouer();

        //Si j'ai un prix minimal alors j'ajoute une condition pour setter le prix min
        if ($rechercheBien->getPrixMin()) {
            $query = $query
                ->andwhere('b.prix >= :prixMin')
                ->setParameter('prixMin', $rechercheBien->getPrixMin());
        }
        //Si j'ai un prix maximal alors j'ajoute une condition pour setter le prix max
        if ($rechercheBien->getPrixMax()) {
            $query = $query
                ->andwhere('b.prix <= :prixMax')
                ->setParameter('prixMax', $rechercheBien->getPrixMax());
        }

        //Si j'ai une surface minimale alors j'ajoute une condition pour setter la surface
        if ($rechercheBien->getSurfaceMin()) {
            $query = $query
                ->andwhere('b.surface >= :surfaceMin')
                ->setParameter('surfaceMin', $rechercheBien->getSurfaceMin());
        }
        //Si j'ai une surface maximale alors j'ajoute une condition pour setter la surface
        if ($rechercheBien->getSurfaceMax()) {
            $query = $query
                ->andwhere('b.surface <= :surfaceMax')
                ->setParameter('surfaceMax', $rechercheBien->getSurfaceMax());
        }

        //Si j'ai un nombre de pièces alors j'ajoute une condition pour setter le nombre de pièces
        if ($rechercheBien->getNbrepiece()) {
            $query = $query
                ->andwhere('b.nbrepiece = :nbrepiece')
                ->setParameter('nbrepiece', $rechercheBien->getNbrepiece());
        }
        //Si j'ai un nombre de chambres alors j'ajoute une condition pour setter le nombre de chambres
        if ($rechercheBien->getNbreChambre()) {
            $query = $query
                ->andwhere('b.nbreChambre = :nbreChambre')
                ->setParameter('nbreChambre', $rechercheBien->getNbreChambre());
        }


        //permet de récupérer les biens en fonction de la catégorie.
        if ($rechercheBien->getCategorie()) {
            $query = $query
                ->andwhere('b.categorie = :categorie')
                ->setParameter('categorie', $rechercheBien->getCategorie());
        }

        if ($rechercheBien->getTypeChauffage()) {
            $query = $query
                ->andwhere('b.typeChauffage = :typeChauffage')
                ->setParameter('typeChauffage', $rechercheBien->getTypeChauffage());
        }

        //permet de récupérer les biens en fonction de l'option demandée.
        if ($rechercheBien->getOptions()->count() > 0) {
            $k = 0;
            foreach ($rechercheBien->getOptions() as $option) {
                $k++;
                $query = $query
                    ->andwhere(":option$k MEMBER OF b.options")
                    ->setParameter("option$k", $option);
            }
        }
        //condition pour ce qui est du calcul de la longitude et la latitude
        if ($rechercheBien->getLatitude() && $rechercheBien->getLongitude() && $rechercheBien->getDistance()) {
            $query = $query
                ->select('b')
                ->andWhere('(6353 * 2 * ASIN(SQRT( POWER(SIN((b.latitude - :latitude) *  pi()/180 / 2), 2) 
                +COS(b.latitude * pi()/180) * COS(:latitude * pi()/180)
                 * POWER(SIN((b.longitude - :longitude) * pi()/180 / 2), 2) ))) <= :distance')
                ->setParameter('longitude', $rechercheBien->getLongitude())
                ->setParameter('latitude', $rechercheBien->getLatitude())
                ->setParameter('distance', $rechercheBien->getDistance());
        }


        return $query->getQuery();
    }

    /**
     * findAllBienaLouer recherche tous les biens avec le statut à vendre pour la partie website
     *
     * @return QueryBuilder
     */
    public function findAllBienaVendre(): QueryBuilder
    {
        return $this->createQueryBuilder('b')
            ->where('b.statusBien = false')
            ->andwhere('b.deletedAt IS NULL')
            ->andwhere('b.typeTransaction=1');
        // ->getQuery()
        // ->getResult();
    }


    /**
     * Répérer en fonction de la recherche les biens dans la partie web de l'application
     * Cette recherche est fonction des informations passer dans le formulaire
     * @param AdminSearchBien $rechercheBien
     * @return Query
     */
    public function findAllBienaVendreFormWebSite(AdminSearchBien $rechercheBien): Query
    {
        //Sauvegarder ma requête dans une variable query pour ajouter des conditions
        $query =  $this->findAllBienaVendre();

        //Si j'ai un prix minimal alors j'ajoute une condition pour setter le prix min
        if ($rechercheBien->getPrixMin()) {
            $query = $query
                ->andwhere('b.prix >= :prixMin')
                ->setParameter('prixMin', $rechercheBien->getPrixMin());
        }
        //Si j'ai un prix maximal alors j'ajoute une condition pour setter le prix max
        if ($rechercheBien->getPrixMax()) {
            $query = $query
                ->andwhere('b.prix <= :prixMax')
                ->setParameter('prixMax', $rechercheBien->getPrixMax());
        }

        //Si j'ai une surface minimale alors j'ajoute une condition pour setter la surface
        if ($rechercheBien->getSurfaceMin()) {
            $query = $query
                ->andwhere('b.surface >= :surfaceMin')
                ->setParameter('surfaceMin', $rechercheBien->getSurfaceMin());
        }
        //Si j'ai une surface maximale alors j'ajoute une condition pour setter la surface
        if ($rechercheBien->getSurfaceMax()) {
            $query = $query
                ->andwhere('b.surface <= :surfaceMax')
                ->setParameter('surfaceMax', $rechercheBien->getSurfaceMax());
        }

        //Si j'ai un nombre de pièces alors j'ajoute une condition pour setter le nombre de pièces
        if ($rechercheBien->getNbrepiece()) {
            $query = $query
                ->andwhere('b.nbrepiece = :nbrepiece')
                ->setParameter('nbrepiece', $rechercheBien->getNbrepiece());
        }
        //Si j'ai un nombre de chambres alors j'ajoute une condition pour setter le nombre de chambres
        if ($rechercheBien->getNbreChambre()) {
            $query = $query
                ->andwhere('b.nbreChambre = :nbreChambre')
                ->setParameter('nbreChambre', $rechercheBien->getNbreChambre());
        }


        //permet de récupérer les biens en fonction de la catégorie.
        if ($rechercheBien->getCategorie()) {
            $query = $query
                ->andwhere('b.categorie = :categorie')
                ->setParameter('categorie', $rechercheBien->getCategorie());
        }

        if ($rechercheBien->getTypeChauffage()) {
            $query = $query
                ->andwhere('b.typeChauffage = :typeChauffage')
                ->setParameter('typeChauffage', $rechercheBien->getTypeChauffage());
        }

        //permet de récupérer les biens en fonction de l'option demandée.
        if ($rechercheBien->getOptions()->count() > 0) {
            $k = 0;
            foreach ($rechercheBien->getOptions() as $option) {
                $k++;
                $query = $query
                    ->andwhere(":option$k MEMBER OF b.options")
                    ->setParameter("option$k", $option);
            }
        }
        //condition pour ce qui est du calcul de la longitude et la latitude
        if ($rechercheBien->getLatitude() && $rechercheBien->getLongitude() && $rechercheBien->getDistance()) {
            $query = $query
                ->select('b')
                ->andWhere('(6353 * 2 * ASIN(SQRT( POWER(SIN((b.latitude - :latitude) *  pi()/180 / 2), 2) 
                +COS(b.latitude * pi()/180) * COS(:latitude * pi()/180)
                 * POWER(SIN((b.longitude - :longitude) * pi()/180 / 2), 2) ))) <= :distance')
                ->setParameter('longitude', $rechercheBien->getLongitude())
                ->setParameter('latitude', $rechercheBien->getLatitude())
                ->setParameter('distance', $rechercheBien->getDistance());
        }


        return $query->getQuery();
    }


    /**
     * Récupérer tous les biens dont la catégorie est une Maison
     *
     * @return void
     */
    public function getTotalMaison()
    {
        return $this->createQueryBuilder('c')
            ->select('count(c.id)')
            ->andWhere('c.categorie = 1')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Récupérer tous les biens dont la catégorie est une Studio
     *
     * @return void
     */
    public function getTotalStudio()
    {
        return $this->createQueryBuilder('c')
            ->select('count(c.id)')
            ->andWhere('c.categorie = 2')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Récupérer tous les biens dont la catégorie est un appartement
     *
     * @return void
     */
    public function getTotalAppartement()
    {
        return $this->createQueryBuilder('c')
            ->select('count(c.id)')
            ->andWhere('c.categorie = 3')
            ->getQuery()
            ->getSingleScalarResult();
    }


    /**
     * Récupérer tous les biens dont la catégorie est une Villa
     *
     * @return void
     */
    public function getTotalVilla()
    {
        return $this->createQueryBuilder('c')
            ->select('count(c.id)')
            ->andWhere('c.categorie = 4')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Récupérer tous les biens dont la catégorie est un Bureau
     *
     * @return void
     */
    public function getTotalBureau()
    {
        return $this->createQueryBuilder('c')
            ->select('count(c.id)')
            ->andWhere('c.categorie = 5')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Récupérer tous les rendez-vous du mois de Janvier
     *
     * @return void
     */
    public function getRdvJanvier()
    {
        return $this->createQueryBuilder('c')
            ->andWhere('SUBSTRING(c.dateVisite,6,2) = 01')
            ->getQuery()
            ->getResult();
    }

    //------------------STATISTIQUES--------------------//
    /**
     * Récupérer toutes les maisons vendu
     *
     * @return void
     */
    public function totalMaisonVendu()
    {
        return $this->createQueryBuilder('c')
            ->select('count(c.id)')
            ->andWhere('c.categorie = 1')
            ->andwhere('c.statusBien = 1')
            ->getQuery()
            ->getSingleScalarResult();
    }
    /**
     * Récupérer toutes les appartements vendu
     *
     * @return void
     */
    public function totalAppartVendu()
    {
        return $this->createQueryBuilder('c')
            ->select('count(c.id)')
            ->andWhere('c.categorie = 2')
            ->andwhere('c.statusBien = 1')
            ->getQuery()
            ->getSingleScalarResult();
    }
    /**
     * Récupérer toutes les studio vendu
     *
     * @return void
     */
    public function totalStudioVendu()
    {
        return $this->createQueryBuilder('c')
            ->select('count(c.id)')
            ->andWhere('c.categorie = 3')
            ->andwhere('c.statusBien = 1')
            ->getQuery()
            ->getSingleScalarResult();
    }
    /**
     * Récupérer toutes les studio vendu
     *
     * @return void
     */
    public function totalVillaVendu()
    {
        return $this->createQueryBuilder('c')
            ->select('count(c.id)')
            ->andWhere('c.categorie = 4')
            ->andwhere('c.statusBien = 1')
            ->getQuery()
            ->getSingleScalarResult();
    }
    /**
     * Récupérer toutes les bureau vendu
     *
     * @return void
     */
    public function totalBureauVendu()
    {
        return $this->createQueryBuilder('c')
            ->select('count(c.id)')
            ->andWhere('c.categorie = 5')
            ->andwhere('c.statusBien = 1')
            ->getQuery()
            ->getSingleScalarResult();
    }
    /**
     * Récupérer toutes les bureau vendu
     *
     * @return void
     */
    public function totalBiensVendus()
    {
        return $this->createQueryBuilder('c')
            ->select('count(c.id)')
            ->andwhere('c.statusBien = 1')
            ->getQuery()
            ->getSingleScalarResult();
    }

    // /**
    //  * @return Biens[] Returns an array of Biens objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Biens
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
