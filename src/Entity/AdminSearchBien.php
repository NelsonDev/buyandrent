<?php
//mise en place du système de filtre des biens dans la partie App Pro
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PropertySearch
 */
class AdminSearchBien
{
    /**
     * prixMax
     * @var int|null
     */
    private $prixMax;

    /**
     * prixMin
     * @Assert\Range( min = 10)
     * @var int|null
     */
    private $prixMin;

    /**
     * surfaceMin
     * @Assert\Range( min = 10)
     * @var int|null
     */
    private $surfaceMin;

    /**
     * surfaceMax
     * @Assert\Range( max = 10000)
     * @var int|null
     */
    private $surfaceMax;

    /**
     * nbrepiece
     * @Assert\Range( min = 1)
     * @var int|null
     */
    private $nbrepiece;

    /**
     * nbrepiece
     *@Assert\Range( min = 1)
     * @var int|null
     */
    private $nbreChambre;

    /**
     * @var ArrayCollection
     */
    private $options;

    /**
     * @var int|null
     */
    private $categorie;

    /**
     * @var string|null
     */
    private $adresse;
    /**
     * @var float|null
     */
    private $latitude;

    /**
     * @var float|null
     */
    private $longitude;

    /**
     * @var int|null
     */
    private $distance;

    /**
     * @var int|null
     */
    private $typeTransaction;

    /**
     * @var int|null
     */
    private $typeChauffage;

    public function __construct()
    {
        $this->options = new ArrayCollection();
    }


    /**
     * Get prixMax
     *
     * @return  int|null
     */
    public function getPrixMax(): ?int
    {
        return $this->prixMax;
    }

    /**
     * Set prixMax
     *
     * @param  mixed  $prixMax  prixMax
     *
     * @return  self
     */
    public function setPrixMax(int $prixMax): AdminSearchBien
    {
        $this->prixMax = $prixMax;

        return $this;
    }

    /**
     * Get surfaceMin
     *
     * @return  int|null
     */
    public function getSurfaceMin(): ?int
    {
        return $this->surfaceMin;
    }

    /**
     * Set surfaceMin
     *
     * @param  mixed  $surfaceMin  surfaceMin
     *
     * @return  self
     */
    public function setSurfaceMin($surfaceMin)
    {
        $this->surfaceMin = $surfaceMin;

        return $this;
    }

    /**
     * Get the value of options
     *
     * @return  ArrayCollection
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set the value of options
     *
     * @param  ArrayCollection  $options
     *
     * @return  self
     */
    public function setOptions(ArrayCollection $options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Get prixMax
     *
     * @return  int|null
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set prixMax
     *
     * @param  int|null  $categorie  prixMax
     *
     * @return  self
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get the value of latitude
     *
     * @return  float|null
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set the value of latitude
     *
     * @param  float|null  $latitude
     *
     * @return  self
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get the value of longitude
     *
     * @return  float|null
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set the value of longitude
     *
     * @param  float|null  $longitude
     *
     * @return  self
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get the value of distance
     *
     * @return  int|null
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * Set the value of distance
     *
     * @param  int|null  $distance
     *
     * @return  self
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;

        return $this;
    }

    /**
     * Get the value of adresse
     *
     * @return  string|null
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set the value of adresse
     *
     * @param  string|null  $adresse
     *
     * @return  self
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get the value of typeTransaction
     *
     * @return  int|null
     */
    public function getTypeTransaction()
    {
        return $this->typeTransaction;
    }

    /**
     * Set the value of typeTransaction
     *
     * @param  int|null  $typeTransaction
     *
     * @return  self
     */
    public function setTypeTransaction($typeTransaction)
    {
        $this->typeTransaction = $typeTransaction;

        return $this;
    }

    /**
     * Get the value of typeChauffage
     *
     * @return  int|null
     */
    public function getTypeChauffage()
    {
        return $this->typeChauffage;
    }

    /**
     * Set the value of typeChauffage
     *
     * @param  int|null  $typeChauffage
     *
     * @return  self
     */
    public function setTypeChauffage($typeChauffage)
    {
        $this->typeChauffage = $typeChauffage;

        return $this;
    }

    /**
     * Get prixMin
     *
     * @return  int|null
     */
    public function getPrixMin()
    {
        return $this->prixMin;
    }

    /**
     * Set prixMin
     *
     * @param  int|null  $prixMin  prixMin
     *
     * @return  self
     */
    public function setPrixMin($prixMin)
    {
        $this->prixMin = $prixMin;

        return $this;
    }

    /**
     * Get surfaceMax
     *
     * @return  int|null
     */
    public function getSurfaceMax()
    {
        return $this->surfaceMax;
    }

    /**
     * Set surfaceMax
     *
     * @param  int|null  $surfaceMax  surfaceMax
     *
     * @return  self
     */
    public function setSurfaceMax($surfaceMax)
    {
        $this->surfaceMax = $surfaceMax;

        return $this;
    }

    /**
     * Get nbrepiece
     *
     * @return  int|null
     */
    public function getNbrepiece()
    {
        return $this->nbrepiece;
    }

    /**
     * Set nbrepiece
     *
     * @param  int|null  $nbrepiece  nbrepiece
     *
     * @return  self
     */
    public function setNbrepiece($nbrepiece)
    {
        $this->nbrepiece = $nbrepiece;

        return $this;
    }

    /**
     * Get nbrepiece
     *
     * @return  int|null
     */
    public function getNbreChambre()
    {
        return $this->nbreChambre;
    }

    /**
     * Set nbrepiece
     *
     * @param  int|null  $nbreChambre  nbrepiece
     *
     * @return  self
     */
    public function setNbreChambre($nbreChambre)
    {
        $this->nbreChambre = $nbreChambre;

        return $this;
    }
}
