<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AgenceRepository")
 * @Vich\Uploadable()
 */
class Agence
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="BuyAndRent_images", fileNameProperty="imageAgence")
     *
     * @var File|null
     * @Assert\Image(
     *     mimeTypes = { "image/png",   "image/jpeg", "image/jpg" })
     * ])
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      min = 2,
     *      max = 255,
     *      minMessage = "Votre adresse doit être au moins  {{ limit }} caractères",
     *      maxMessage = "Votre adresse doit être  au plus {{ limit }} caractères"
     * )
     * @Assert\NotBlank
     */
    private $denomination;

    /**
     * @ORM\Column(type="text")
     * @Assert\Length(
     *      min = 10,
     *      max = 5000,
     *      minMessage = "Votre description doit contenir au moins {{ limit }} caractères",
     *      maxMessage = "Votre description doit contenir au plus{{ limit }} caractères"
     * )
     * @Assert\NotBlank
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Email(message = "Cette adresse email n'est pas valide.")
     * @Assert\NotBlank
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex("/^[0-9]{10}$/",
     *  message="Le numero de téléphone doit être de sur ce format : 0625154078"
     * )
     * @Assert\NotBlank
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      min = 2,
     *      max = 255,
     *      minMessage = "Votre adresse doit être au moins  {{ limit }} caractères",
     *      maxMessage = "Votre adresse doit être  au plus {{ limit }} caractères"
     * )
     * @Assert\NotBlank
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex("/^[0-9]{5}$/",
     * message="L'adresse postal doit contenir 5 chiffres exemple : 75000")
     * @Assert\NotBlank
     */
    private $codePostal;

    /**
     * @ORM\Column(type="string", length=255)
     *@Assert\Length(
     *      min = 2,
     *      max = 255,
     *      minMessage = "La ville doit être au moins  {{ limit }} caractères",
     *      maxMessage = "La ville doit être au  plus {{ limit }} caractères"
     * )
     * @Assert\NotBlank
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageAgence;

    /**
     * @ORM\Column(type="datetime" , nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $couleur_1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $couleur_2;


    public function __construct()
    {
        $this->setCreatedAt(new DateTime());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDenomination(): ?string
    {
        return $this->denomination;
    }

    public function setDenomination(string $denomination): self
    {
        $this->denomination = $denomination;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    public function setCodePostal(string $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getImageAgence(): ?string
    {
        return $this->imageAgence;
    }

    public function setImageAgence(?string $imageAgence): void
    {
        $this->imageAgence = $imageAgence;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get nOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @return  File|null
     */
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * Set nOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @param  File|null  $imageFile  NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @return  self
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            $this->updatedAt = new \DateTimeImmutable();
        }
    }
    public function getCouleur1(): ?string
    {
        if ($this->couleur_1 == null) {
            $this->couleur_1 = "#b4cc83";
        }
        return $this->couleur_1;
    }

    public function setCouleur1(?string $couleur_1): self
    {
        $this->couleur_1 = $couleur_1;
        return $this;
    }

    public function getCouleur2(): ?string
    {
        if ($this->couleur_2 == null) {
            $this->couleur_2 = "#154854";
        }
        return $this->couleur_2;
    }

    public function setCouleur2(?string $couleur_2): self
    {
        $this->couleur_2 = $couleur_2;

        return $this;
    }
}
