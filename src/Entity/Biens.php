<?php

namespace App\Entity;

use Datetime;
use App\Entity\Image;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BiensRepository")
 * @UniqueEntity("titre")
 */
class Biens
{
    //Constante pour préciser le type de chauffage pour chaque bien.
    const CHAUFFAGE = [
        0 => 'Electrique',
        1 => 'Gaz',
        2 => 'A prévoir',
        3 => 'Pas de chauffage'
    ];

    //Constance pour specifier le type de transaction
    const TRANSACTION = [
        0 => 'Location',
        1 => 'Vente'
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */

    private $id;


    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      min = 5,
     *      max = 255,
     *      minMessage = "{{ limit }} caractères au moins",
     *      maxMessage = "{{ limit }} caractères  au plus"
     * )
     * @Assert\NotBlank
     */
    private $titre;

    /**
     * @ORM\Column(type="text")
     * @Assert\Length(
     *      min = 10,
     *      minMessage = "Description très courte  {{ limit }} caractères au moins",
     * )
     * @Assert\NotBlank
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(
     *      min = 10,
     *      max = 100000,
     *      notInRangeMessage = "La surface doit être comprise entre {{ min }} {{ max }}m²"
     * )
     * @Assert\NotBlank
     */
    private $surface;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    private $nbrepiece;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    private $nbreChambre;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\NotBlank
     */
    private $etage;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    private $typeChauffage;

    /**
     * @ORM\Column(type="boolean" , options={"default": false})
     */
    private $statusBien = false;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      min = 3,
     *      max = 255,
     *      minMessage = "Prénom trop court,  {{ limit }} caractères au moins",
     *      maxMessage = "Prénom trop long, {{ limit }} caractères  au plus"
     * )
     * @Assert\NotBlank
     */
    private $proprioPrenom;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      min = 3,
     *      max = 255,
     *      minMessage = "Nom trop court,  {{ limit }} caractères au moins",
     *      maxMessage = "Nom trop long, {{ limit }} caractères  au plus"
     * )
     * @Assert\NotBlank
     */
    private $proprioNom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Regex("/^[0-9]{10}$/",
     *  message="Le numero de téléphone doit être de sur ce format : 0625154078"
     * )
     *@Assert\NotBlank
     */
    private $proprioNumero;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Email(message = "Cette adresse email n'est pas valide.")
     * @Assert\NotBlank
     */
    private $proprioEmail;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    private $prix;

    /**
     * @ORM\Column(type="string", length=255)
     *@Assert\Length(
     *      min = 2,
     *      max = 255,
     *      minMessage = "La ville doit être au moins  {{ limit }} caractères",
     *      maxMessage = "La ville doit être au  plus {{ limit }} caractères"
     * )
     * @Assert\NotBlank
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      min = 2,
     *      max = 255,
     *      minMessage = "Votre adresse doit être au moins  {{ limit }} caractères",
     *      maxMessage = "Votre adresse doit être  au plus {{ limit }} caractères"
     * )
     * @Assert\NotBlank
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex("/^[0-9]{5}$/",
     * message="L'adresse postal doit contenir 5 chiffres exemple : 75000")
     *@Assert\NotBlank
     */
    private $codePostal;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $referenceBien;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categories", inversedBy="biens")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categorie;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Users", inversedBy="biens")
     */
    private $users;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $chargeComprise = false;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    private $typeTransaction;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Option", inversedBy="biens")
     */
    private $options;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Image", mappedBy="biens", orphanRemoval=true, cascade={"persist"})
     */
    private $images;


    /**
     * @Assert\All({
     *   @Assert\Image( mimeTypes = { "image/png",   "image/jpeg", "image/jpg" })
     * })
     *
     */

    private $imageFiles;

    /**
     * La précision de la latitude va de 0-90
     * @ORM\Column(type="float", scale =4 , precision=6 )
     */
    private $latitude;

    /**
     * La précision de la longitude va de -180-180
     * @ORM\Column(type="float", scale =4 , precision=7 )
     */
    private $longitude;

    /**
     * getFormaterPrix pour factoriser le prix des biens (20000 => 20 000)
     *
     * @return string
     */
    public function getFormaterPrix(): string
    {
        return number_format($this->prix, 0, '', ' ');
    }

    /**
     * getSlug le titre de nos publications (titre du bien => titre-du-bien)
     *
     * @param SluggerInterface $slugger
     * @return string
     */
    public function getSlug(): string
    {
        return (new Slugify())->Slugify($this->titre);
    }


    public function __construct()
    {
        $this->setCreatedAt(new DateTime());
        $this->options = new ArrayCollection();
        $this->images = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSurface(): ?int
    {
        return $this->surface;
    }

    public function setSurface(int $surface): self
    {
        $this->surface = $surface;

        return $this;
    }

    public function getNbrepiece(): ?int
    {
        return $this->nbrepiece;
    }

    public function setNbrepiece(int $nbrepiece): self
    {
        $this->nbrepiece = $nbrepiece;

        return $this;
    }

    public function getNbreChambre(): ?int
    {
        return $this->nbreChambre;
    }

    public function setNbreChambre(int $nbreChambre): self
    {
        $this->nbreChambre = $nbreChambre;

        return $this;
    }

    public function getEtage(): ?int
    {
        return $this->etage;
    }

    public function setEtage(?int $etage): self
    {
        $this->etage = $etage;

        return $this;
    }

    public function getTypeChauffage(): ?int
    {
        return $this->typeChauffage;
    }

    public function setTypeChauffage(int $typeChauffage): self
    {
        $this->typeChauffage = $typeChauffage;

        return $this;
    }

    public function getStatusBien(): ?bool
    {
        return $this->statusBien;
    }

    public function setStatusBien(bool $statusBien): self
    {
        $this->statusBien = $statusBien;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getProprioPrenom(): ?string
    {
        return $this->proprioPrenom;
    }

    public function setProprioPrenom(string $proprioPrenom): self
    {
        $this->proprioPrenom = $proprioPrenom;

        return $this;
    }

    public function getProprioNom(): ?string
    {
        return $this->proprioNom;
    }

    public function setProprioNom(string $proprioNom): self
    {
        $this->proprioNom = $proprioNom;

        return $this;
    }

    public function getProprioNumero(): ?string
    {
        return $this->proprioNumero;
    }

    public function setProprioNumero(?string $proprioNumero): self
    {
        $this->proprioNumero = $proprioNumero;

        return $this;
    }

    public function getProprioEmail(): ?string
    {
        return $this->proprioEmail;
    }

    public function setProprioEmail(string $proprioEmail): self
    {
        $this->proprioEmail = $proprioEmail;

        return $this;
    }

    public function getPrix(): ?int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    public function setCodePostal(string $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getReferenceBien(): ?string
    {
        return $this->referenceBien;
    }

    public function setReferenceBien(?string $referenceBien): self
    {
        $this->referenceBien = $referenceBien;

        return $this;
    }

    public function getCategorie(): ?Categories
    {
        return $this->categorie;
    }

    public function setCategorie(?Categories $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getUsers(): ?Users
    {
        return $this->users;
    }

    public function setUsers(?Users $users): self
    {
        $this->users = $users;

        return $this;
    }

    public function getChargeComprise(): ?bool
    {
        return $this->chargeComprise;
    }

    public function setChargeComprise(bool $chargeComprise): self
    {
        $this->chargeComprise = $chargeComprise;

        return $this;
    }

    public function getTypeTransaction(): ?int
    {
        return $this->typeTransaction;
    }

    public function setTypeTransaction(int $typeTransaction): self
    {
        $this->typeTransaction = $typeTransaction;

        return $this;
    }

    /**
     * @return Collection|Option[]
     */
    public function getOptions(): Collection
    {
        return $this->options;
    }

    //Ajouter des options dans le bien
    public function addOption(Option $option): self
    {
        if (!$this->options->contains($option)) {
            $this->options[] = $option;
            $option->addBien($this);
        }

        return $this;
    }

    public function removeOption(Option $option): self
    {
        if ($this->options->contains($option)) {
            $this->options->removeElement($option);
            $option->removeBien($this);
        }

        return $this;
    }

    /**
     * @return Collection|Image[]
     *
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setBiens($this);
        }

        return $this;
    }

    //Renvoyer la première image de notre collection.
    public function getImage(): ?Image
    {
        if ($this->images->isEmpty()) {
            return null;
        }
        return $this->images->first();
    }


    public function removeImage(Image $image): self
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
            // set the owning side to null (unless already changed)
            if ($image->getBiens() === $this) {
                $image->setBiens(null);
            }
        }

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getImageFiles()
    {
        return $this->imageFiles;
    }


    /**
     *
     * @return  Biens
     */
    public function setImageFiles($imageFiles)
    {
        //parcourir les images avant d'insérer
        foreach ($imageFiles as $imageFile) {
            $image = new Image();
            $image->setImageFile($imageFile);
            $this->addImage($image);
        }
        $this->imageFiles = $imageFiles;

        return $this;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }
}
