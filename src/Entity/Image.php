<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImageRepository")
 * @Vich\Uploadable()
 *
 */
class Image
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var File|null
     * @Assert\Image(
     *     mimeTypes = { "image/png", "image/jpeg", "image/jpg" })
     * @Vich\UploadableField(mapping="BuyAndRent_images", fileNameProperty="imageBien")
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $imageBien;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Biens", inversedBy="images")
     * @ORM\JoinColumn(nullable=false)
     */
    private $biens;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImageBien(): ?string
    {
        return $this->imageBien;
    }

    public function setImageBien(?string $imageBien): self
    {
        $this->imageBien = $imageBien;

        return $this;
    }

    public function getBiens(): ?Biens
    {
        return $this->biens;
    }

    public function setBiens(?Biens $biens): self
    {
        $this->biens = $biens;

        return $this;
    }


    /**
     * Get mimeTypes = { "image/png", "image/jpeg", "image/jpg" })
     *
     * @return null|File
     */
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * Set mimeTypes = { "image/png", "image/jpeg", "image/jpg" })
     *
     * @return  File|null
     *
     */
    public function setImageFile(?File $imageFile): self
    {
        $this->imageFile = $imageFile;

        return $this;
    }
}
