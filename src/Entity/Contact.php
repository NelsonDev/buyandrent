<?php

namespace App\Entity;

use App\Entity\Biens;
use Symfony\Component\Validator\Constraints as Assert;

class Contact
{
    /**
     * @var string|null
     * @Assert\NotBlank()
     * @Assert\Length(min=3, max=50)
     *
     */
    private $prenom;


    /**
     * @var string|null
     * @Assert\NotBlank()
     * @Assert\Length(min=3, max=50)
     *
     */
    private $nom;


    /**
     * @var string|null
     * @Assert\Length(min=3, max=30)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;


    /**
     * @var string|null
     * @Assert\NotBlank()
     * @Assert\Regex(
     *  pattern="/[0-9]{10}/"
     * )
     *
     */
    private $telephone;


    /**
     * @var string|null
     * @Assert\NotBlank()
     * @Assert\Length(min=10)
     */
    private $message;


    /**
     * @var Biens|null
     */
    private $bien;


    /**
     * Get the value of nom
     *
     * @return  string|null
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @param  string|null  $nom
     *
     * @return  self
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get the value of prenom
     *
     * @return  string|null
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set the value of prenom
     *
     * @param  string|null  $prenom
     *
     * @return  self
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get the value of email
     *
     * @return  string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @param  string|null  $email
     *
     * @return  self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get pattern="/[0-9]{10}/"
     *
     * @return  string|null
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set pattern="/[0-9]{10}/"
     *
     * @param  string|null  $telephone  pattern="/[0-9]{10}/"
     *
     * @return  self
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get the value of message
     *
     * @return  string|null
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set the value of message
     *
     * @param  string|null  $message
     *
     * @return  self
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get the value of bien
     *
     * @return  Biens|null
     */
    public function getBien()
    {
        return $this->bien;
    }

    /**
     * Set the value of bien
     *
     * @param  Biens|null  $bien
     *
     * @return  self
     */
    public function setBien($bien)
    {
        $this->bien = $bien;

        return $this;
    }
}
